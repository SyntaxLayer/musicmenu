# MusicMenu
*Play your own music in GTA V*
## Features
### Local files
- The menu structure is created automatically according to your file structure. This means that the file located at *folder1/folder2/myFile.mp3* will be in *submenu1/submenu2/myFile* inside the menu structure.
- Playback queue.
- Playlist creation.
- Play songs in loop.
- Play random song. The menu comes with an option that once selected, will play a random song from any of your subfolders.
- Keep playing. If active, the script will keep playing random songs if the playback queue is empty.
- Skip songs.
- Skip song time forwards or backwards.
### Spotify (from version 2.0.0)
- Resume/Pause playback (requires premium)
- Set active device (requires premium)
- Browse saved playlists and albums
- Play or queue tracks from saved playlists and albums (requires premium)
- Play the entire album or playlist (requires premium)
- Play next or previous song (requires premium)
- Change volume (requires premium)
- Set repeat mode (requires premium)
- Toggle shuffle mode (requires premium)
- Browse saved tracks
- Play or queue saved tracks (requires premium)

## Requirements
- [LemonUI](https://github.com/LemonUIbyLemon/LemonUI/releases)
- [Script Hook V .Net](https://github.com/crosire/scripthookvdotnet/releases)
- [Script Hook V](http://dev-c.com/gtav/scripthookv)

## Installation
Copy the contents of the compressed file to your scripts folder.

*Music files must be **mp3** format and located in the **MusicMenu/music** folder!*

*As explained in the features section, there can be subfolders inside the music folder.*
## Controls
Press **CTRL + Z** to open the menu. *Can be edited in the config.toml file.*

## Credits
- Script by Me
- SHVDN by Crosire
- LemonUI by Lemon
- SHV by Alexander Blade
- Idea by [JohnFromGWN](https://www.gta5-mods.com/users/JohnFromGWN)
