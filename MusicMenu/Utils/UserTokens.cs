namespace MusicMenu.Utils;

public struct Tokens
{
    public static string? AccessToken;
    public static string? RefreshToken;
}