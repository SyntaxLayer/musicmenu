using System.IO;
using Tommy;

namespace MusicMenu.Utils;

public static class ScriptConfig
{
    public static void ReadConfig()
    {
        using var reader = File.OpenText($"{Directory.GetCurrentDirectory()}\\scripts\\MusicMenu\\config.toml");
        var table = TOML.Parse(reader);
        Main.OpenMenuA = table["Controls"]["OpenMenuA"];
        Main.OpenMenuB = table["Controls"]["OpenMenuB"];
        Main.QueueSong = table["Controls"]["QueueSong"];
        Main.AddToPlaylist = table["Controls"]["AddToPlaylist"];
        Main.SavePlaylist = table["Controls"]["SavePlaylist"];
        Main.QueueAll = table["Controls"]["QueueAll"];
        Tokens.AccessToken = table["Spotify"]["AccessToken"];
        Tokens.RefreshToken = table["Spotify"]["RefreshToken"];
    }

    public static void UpdateFile()
    {
        TomlTable table = new();
        table["Controls"]["OpenMenuA"] = Main.OpenMenuA;
        table["Controls"]["OpenMenuB"] = Main.OpenMenuB;
        table["Controls"]["QueueSong"] = Main.QueueSong;
        table["Controls"]["AddToPlaylist"] = Main.AddToPlaylist;
        table["Controls"]["SavePlaylist"]=Main.SavePlaylist;
        table["Controls"]["QueueAll"] = Main.QueueAll;
        table["Spotify"]["AccessToken"]= Tokens.AccessToken;
        table["Spotify"]["RefreshToken"] = Tokens.RefreshToken;

        using var writer = File.CreateText("out.toml");
        table.WriteTo(writer);
        writer.Flush();
    }
}