using Newtonsoft.Json;

namespace MusicMenu.Utils.Types.Spotify;

public class Device
{
    [JsonProperty("id", Required = Required.Always)]
    public string Id { get; set; }
    
    [JsonProperty("is_active", Required = Required.Always)]
    public bool IsActive { get; set; }
    
    [JsonProperty("is_private_session", Required = Required.Always)]
    public bool IsActiveSession { get; set; }
    
    [JsonProperty("is_restricted", Required = Required.Always)]
    public bool IsRestricted { get; set; }
    
    [JsonProperty("name", Required = Required.Always)]
    public string Name { get; set; }
    
    [JsonProperty("volume_percent", Required = Required.Always)]
    public int VolumePercent { get; set; }
    
    [JsonConstructor]
    public Device() {}
}