using Newtonsoft.Json;

namespace MusicMenu.Utils.Types.Requests;

public class SpotifyItem
{
    [JsonProperty("id", Required = Required.Always)]
    public string Id;

    [JsonProperty("name", Required = Required.Always)]
    public string Name;

    [JsonProperty("uri", Required = Required.Always)]
    public string Uri;
    
    [JsonConstructor]
    public SpotifyItem() {}
}