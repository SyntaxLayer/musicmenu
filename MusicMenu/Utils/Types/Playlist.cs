using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;

namespace MusicMenu.Utils.Types;

public class Playlist
{
    [JsonProperty("playlistName", Required = Required.Always)]
    public string PlaylistName { get; set; }
    [JsonProperty("songs", Required = Required.Always)]
    public List<string> Songs { get; set; }
    
    [JsonConstructor]
    public Playlist() { }

    public static List<Playlist>? ReadFromDisk()
    {
        var content = File.ReadAllText($"{Directory.GetCurrentDirectory()}\\scripts\\MusicMenu\\playlists.json");
        return JsonConvert.DeserializeObject<List<Playlist>>(content);
    }
}