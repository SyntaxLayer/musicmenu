using System.Collections.Generic;
using MusicMenu.Utils.Types.Requests;
using Newtonsoft.Json;

namespace MusicMenu.Utils.Types.Spotify;

public class ItemPage
{
    [JsonProperty("items", Required = Required.Always)]
    public List<SpotifyItem> Items;

    [JsonProperty("offset", Required = Required.Always)]
    public int Offset;

    [JsonProperty("next", Required = Required.Always)]
    public bool Next;
    
    [JsonConstructor]
    public ItemPage() {}
}