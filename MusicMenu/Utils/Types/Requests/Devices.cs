using System.Collections.Generic;
using MusicMenu.Utils.Types.Spotify;
using Newtonsoft.Json;

namespace MusicMenu.Utils.Types.Requests;

public class Devices
{
    [JsonProperty("devices", Required = Required.Always)] 
    public List<Device> UserDevices;
    
    [JsonConstructor]
    public Devices() {}
}