using Newtonsoft.Json;

namespace MusicMenu.Utils.Types.Requests;

public class Message
{
    [JsonProperty("msg", Required = Required.Always)]
    public string Text;

    [JsonProperty("token", Required = Required.Default)]
    public string? Token;
    
    [JsonProperty("status", Required = Required.Default)]
    public int? Status;
    
    [JsonConstructor]
    public Message() {}
}