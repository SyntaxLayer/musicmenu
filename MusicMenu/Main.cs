﻿using System;
using GTA;
using GTA.Native;
using MusicMenu.Interface;
using MusicMenu.Helpers;
using MusicMenu.Utils;

namespace MusicMenu
{
    public class Main : Script
    {
        private readonly Menu _musicMenu;
        private readonly ProgressBar _progressBar;
        public static int OpenMenuA { get; set; }
        public static int OpenMenuB { get; set; }
        public static int QueueSong { get; set; }
        public static int QueueAll { get; set; }
        public static int AddToPlaylist { get; set; }
        public static int SavePlaylist { get; set; }
        private bool _pausedByScript;
        
        public Main()
        {
            ScriptConfig.ReadConfig();
            _musicMenu = new();
            _progressBar = new();
            
            Tick += OnTick;
        }

        private void OnTick(object sender, EventArgs e)
        {
            /*if (Game.IsControlPressed(Control.Context))
            {
                var task = RequestHandler.GetDevices();
                task.Start();
            }*/
            
            if (Music.Playing())
            {
                //Doesn't work
                switch (Game.IsPaused)
                {
                    case true when Music.IsPlaying():
                        _pausedByScript = true;
                        Music.Pause();
                        break;
                    case false when Music.IsPaused() && _pausedByScript:
                        _pausedByScript = false;
                        Music.Resume();
                        break;
                }
            }
            
            _musicMenu.Process();
            _progressBar.Process(_musicMenu.Visible());
            if (Function.Call<bool>(Hash.IS_CONTROL_PRESSED, 0, OpenMenuA) && Function.Call<bool>(Hash.IS_CONTROL_JUST_PRESSED, 0, OpenMenuB) && !_musicMenu.Visible())
            {
                Menu.Open();
            }
        }
    }
}