using System.Collections.Generic;
using System.ComponentModel;
using GTA;
using GTA.Native;
using LemonUI;
using LemonUI.Menus;
using MusicMenu.Helpers;
using MusicMenu.Utils.Types.Requests;

namespace MusicMenu.Interface;

public enum CollectionType
{
    Playlist,
    Album,
    SavedTracks
}

public class TrackCollection : NativeMenu
{
    private readonly SpotifyItem? _data;
    private readonly int _offset;
    private bool _next;
    private readonly ObjectPool _pool;
    private readonly CollectionType _type;
    private List<SpotifyItem> _tracks = new();
    public static string RequestMessage { get; set; } = "";
    private bool TracksUpdated { get; set; }
    private bool PlayingTrack { get; set; }
    private bool QueueingTrack { get; set; }

    private CollectionType GetType(CollectionListType parentType)
    {
        return parentType switch
        {
            CollectionListType.Playlists => CollectionType.Playlist,
            _ => CollectionType.Album
        };
    }
    
    public TrackCollection(
        SpotifyItem data, int offset, CollectionListType parentType, ObjectPool pool) : base(data.Name, data.Name)
    {
        _data = data;
        _type = GetType(parentType);
        _offset = offset;
        _pool = pool;
        _pool.Add(this);
        AddMenuControls();
        
        Opening += OnOpening;
    }
    
    private TrackCollection(
        SpotifyItem data, int offset, CollectionType type, ObjectPool pool) : base(data.Name, data.Name)
    {
        _data = data;
        _type = type;
        _offset = offset;
        _pool = pool;
        _pool.Add(this);
        AddMenuControls();
        
        if (offset > 0)
        {
            Title.Text = "Next";
            Subtitle = "Next";

            Opening += (_, _) =>
            {
                Title.Text = data.Name;
                Subtitle = $"Page: {(_offset / 50) + 1}";
            };

            Closing += (_, _) =>
            {
                Title.Text = "Next";
                Subtitle = "Next";
            };
        }
        
        Opening += OnOpening;
    }

    public TrackCollection(string title, int offset, CollectionType type, ObjectPool pool) : base(title, title)
    {
        _type = type;
        _offset = offset;
        _pool = pool;
        _pool.Add(this);
        AddMenuControls();
        
        if (offset > 0)
        {
            Title.Text = "Next";
            Subtitle = "Next";

            Opening += (_, _) =>
            {
                Title.Text = title;
                Subtitle = $"Page: {(_offset / 50) + 1}";
            };

            Closing += (_, _) =>
            {
                Title.Text = "Next";
                Subtitle = "Next";
            };
        }
        
        Opening += OnOpening;
    }
    
    private void AddMenuControls()
    {
        if (!RequiredControls.Contains((Control)Main.QueueSong))
        {
            Buttons.Add(new("Play", (Control)Main.QueueSong));
            RequiredControls.Add((Control)Main.QueueSong);
        }
            
        if (!RequiredControls.Contains((Control)Main.QueueAll))
        {
            Buttons.Add(new("Queue", (Control)Main.QueueAll));
            RequiredControls.Add((Control)Main.QueueAll);
        }
    }
    
    private void UpdateCollection()
    {
        Clear();
        foreach (var track in _tracks)
        {
            Add(new TrackItem(track.Name, track.Id, track.Uri));
        }

        switch (_next)
        {
            case true when _data is not null:
            {
                AddSubMenu(new TrackCollection(_data, _offset + 50, _type, _pool));
                break;
            }
            case true when _data is null:
            {
                AddSubMenu(new TrackCollection("Saved tracks", _offset + 50, _type, _pool));
                break;
            }
        }
        
        GTA.UI.Notification.Show(RequestMessage, true);
    }

    private async void RunFetch()
    {
        var tracksFetch = _type switch
        {
            CollectionType.Playlist => await RequestHandler.FetchPlaylistTracks(_data!.Id, _offset),
            CollectionType.Album => await RequestHandler.FetchAlbumTracks(_data!.Id, _offset),
            _ => await RequestHandler.FetchTracks(_offset)
        };

        if (tracksFetch == null)
        {
            _tracks.Clear();
            _next = false;
        }
        else
        {
            _tracks = tracksFetch.Items;
            _next = tracksFetch.Next;
        }

        TracksUpdated = true;
    }
    
    private void OnOpening(object sender, CancelEventArgs ev)
    {
        GTA.UI.Notification.Show("Fetching tracks...", true);
        RunFetch();
    }

    private async void RunPlay(string uri)
    {
        await RequestHandler.SendPlay(null, uri);
        PlayingTrack = true;
    }
    
    private async void RunQueue(string uri)
    {
        await RequestHandler.SendQueue(uri);
        QueueingTrack = true;
    }
    
    public override void Process()
    {
        if (TracksUpdated)
        {
            UpdateCollection();
            TracksUpdated = false;
        }

        if (_tracks.Count > 0)
        {
            if (PlayingTrack)
            {
                GTA.UI.Notification.Show(RequestMessage);
                PlayingTrack = false;
            }

            if (QueueingTrack)
            {
                GTA.UI.Notification.Show(RequestMessage);
                QueueingTrack = false;
            }
        
            if (Function.Call<bool>(Hash.IS_CONTROL_JUST_PRESSED, 0, Main.QueueSong) && Visible)
            {
                var item = _tracks[SelectedIndex];
                GTA.UI.Notification.Show($"Playing {item.Name}...");
                RunPlay(item.Uri);
            }
        
            if (Function.Call<bool>(Hash.IS_CONTROL_JUST_PRESSED, 0, Main.QueueAll) && Visible)
            {
                var item = _tracks[SelectedIndex];
                GTA.UI.Notification.Show($"Queueing {item.Name}...");
                RunQueue(item.Uri);
            }
        }
        
        base.Process();
    }
}