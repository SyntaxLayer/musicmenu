using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using GTA;
using GTA.Native;
using LemonUI;
using LemonUI.Elements;
using LemonUI.Menus;
using MusicMenu.Helpers;
using MusicMenu.Utils.Types;
using NAudio.Wave;
using Newtonsoft.Json;

namespace MusicMenu.Interface
{
    public class Menu
    {
        private enum ItemState
        {
            Current,
            Queued,
            Selected,
            None
        }
        
        private enum BadgeSide
        {
            Left,
            Right
        }
        
        //flag used to only update the menu on the first Shown event
        private bool _update;
        private readonly ObjectPool _pool;
        private static NativeMenu _mainMenu;
        private static NativeItem _stopButton;
        private NativeItem _nextButton;
        private static NativeSliderItem _volumeSlider;
        public static bool ShowProgress { get; private set; } = true;
        //flag used to check whether the music stopped by itself (reached the end) or was stopped by the player
        private static bool _triggeredByPlayer;
        private static bool _loop;
        private static int _skipValue;
        private NativeItem? _selectedSong;
        private NativeMenu? _selectedMenu;
        private static bool _showNextNoti; //flag used to show the "Now playing" notification
        private static bool _shouldKeepPlaying;
        private readonly List<string> _playlistSelection = new();
        private List<Playlist> _playlists;
        private bool _inPlaylistMenu;

        private static void ToggleUi(bool on)
        {
            if (on)
            {
                _stopButton.Enabled = true;
                _stopButton.Description = "Stop current song";
                _mainMenu.Subtitle = $"Now playing: {Music.GetLastPlayedName()}";
            }
            else
            {
                _stopButton.Enabled = false;
                _stopButton.Description = "No songs playing.";
                _mainMenu.Subtitle = "";
            }
        }
        
        //Update the menu when the music stops
        public static void WaveOutOnPlaybackStopped(object sender, StoppedEventArgs e)
        {
            switch (_triggeredByPlayer)
            {
                //Repeat the same song only when this event wasn't triggered by the player
                case false when _loop:
                    Music.PlaySong(Music.GetLastPlayedPath());
                    _showNextNoti = true;
                    return;
                //Play the next song in the queue if it isn't empty
                //The loop option has priority over the queue
                case false when Music.Queue.Count > 0:
                    Music.PlaySong(Music.Queue[0]);
                    Music.Queue.RemoveAt(0);
                    ToggleUi(true);
                    _showNextNoti = true;
                    return;
                //Play a random song next
                //The queue has priority over this option
                case false when _shouldKeepPlaying:
                    PlayRandomSong();
                    return;
                case true:
                    //Reset the flag
                    _triggeredByPlayer = false;
                    break;
            }
            
            ToggleUi(false);
        }

        private static void SetItemState(NativeItem item, ItemState state, BadgeSide side = BadgeSide.Right)
        {
            if (side == BadgeSide.Right)
            {
                item.RightBadge = state switch
                {
                    ItemState.Current => new ScaledTexture("mpinventory", "mp_specitem_data")
                    {
                        Color = Color.Lime
                    },
                    ItemState.Queued => new ScaledTexture("commonmenu", "shop_chips_b")
                    {
                        Color = Color.Gray
                    },
                    _ => null //Remove the badge
                };

                return;
            }
            
            item.LeftBadge = state switch
            {
                ItemState.Selected => new ScaledTexture("commonmenu", "shop_new_star")
                {
                    Color = Color.Gold
                },
                _ => null //Remove the badge
            };
        }

        private static string? GetRandomSong()
        {
            var files = Directory.GetFiles(
                $"{Directory.GetCurrentDirectory()}\\scripts\\MusicMenu\\music", 
                "*mp3",
                SearchOption.AllDirectories);
            var random = new Random();
            return files.Length == 0 ? null : files[random.Next(0, files.Length)];
        }
        
        private static void PlayRandomSong()
        {
            var song = GetRandomSong();
            if (song == null) return;

            _triggeredByPlayer = true;
            Music.PlaySong(song);
            ToggleUi(true);
        }

        private void AddMenuControls(NativeMenu menu, bool queue, bool playlist)
        {
            if (queue)
            {
                if (!menu.RequiredControls.Contains((Control)Main.QueueSong))
                {
                    menu.Buttons.Add(new("Add/Remove from Queue", (Control)Main.QueueSong));
                    menu.RequiredControls.Add((Control)Main.QueueSong);
                }
            
                if (!menu.RequiredControls.Contains((Control)Main.QueueAll))
                {
                    menu.Buttons.Add(new("Queue all songs", (Control)Main.QueueAll));
                    menu.RequiredControls.Add((Control)Main.QueueAll);
                }
            }

            if (playlist)
            {
                if (!menu.RequiredControls.Contains((Control)Main.AddToPlaylist))
                {
                    menu.Buttons.Add(new("Add/Remove from playlist selection", (Control)Main.AddToPlaylist));
                    menu.RequiredControls.Add((Control)Main.AddToPlaylist);
                }
            
                if (!menu.RequiredControls.Contains((Control)Main.SavePlaylist))
                {
                    menu.Buttons.Add(new("Save playlist selection", (Control)Main.SavePlaylist));
                    menu.RequiredControls.Add((Control)Main.SavePlaylist);
                }
            }
        }

        private void RestoreItemsState(NativeMenu menu)
        {
            _selectedMenu = menu;
            menu.Items.ForEach(item =>
            {
                switch (item.Enabled)
                {
                    case false when (string)item.Tag != Music.GetLastPlayedPath() || !Music.Playing():
                        item.Enabled = true;
                        SetItemState(item, ItemState.None);
                        break;
                    case true when !Music.Queue.Contains((string)item.Tag) && item.RightBadge != null:
                        SetItemState(item, ItemState.None);
                        break;
                    case true when (string)item.Tag == Music.GetLastPlayedPath() && Music.Playing():
                        item.Enabled = false;
                        SetItemState(item, ItemState.Current);
                        break;
                }

                if (_playlistSelection.Contains((string)item.Tag))
                    SetItemState(item, ItemState.Selected, BadgeSide.Left);
                else if (item.LeftBadge != null)
                    SetItemState(item, ItemState.None, BadgeSide.Left);
            });
        }

        private void RestoreItemState(NativeItem item)
        {
            if (Music.Playing() && Music.GetLastPlayedPath() == (string)item.Tag)
            {
                item.Enabled = false;
                SetItemState(item, ItemState.Current);
            }
            else if (Music.Queue.Contains((string)item.Tag))
            {
                SetItemState(item, ItemState.Queued);
            }
            
            if (_playlistSelection.Contains((string)item.Tag))
                SetItemState(item, ItemState.Selected, BadgeSide.Left);
        }

        private static void PlaySelectedItem(NativeItem item, NativeMenu parentMenu)
        {
            if (Music.Playing()) _triggeredByPlayer = true; //The music stop event was triggered by the player

            //If another song in the same sub menu is playing, reset its item to the default state
            parentMenu.Items.ForEach(songItem =>
            {
                if (songItem.Enabled) return;
                songItem.Enabled = true;
                SetItemState(songItem, ItemState.None);
            });
                    
            //Disable to prevent the player from starting the same song if it's already playing
            item.Enabled = false;
            SetItemState(item, ItemState.Current);
            Music.Queue.Remove((string)item.Tag);
            Music.PlaySong((string)item.Tag);
            ToggleUi(true);
        }
        
        //Recursive function that adds all the submenus and songs
        private void PopulateMenu(string dir, NativeMenu parentMenu)
        {
            var songs = Directory.GetFiles(dir, "*mp3"); //Get song files in current directory
            var subDirs = Directory.GetDirectories(dir); //Get sub directories
            AddMenuControls(parentMenu, true, true);

            parentMenu.Closing += (_, _) =>
            {
                //Delete the saved reference, if any. This will prevent any errors if the menu is closed.
                _selectedSong = null;
                _selectedMenu = null;
            };

            parentMenu.Shown += (_, _) =>
            {
                RestoreItemsState(parentMenu);
            };
            
            //Loop through songs
            foreach (var song in songs)
            {
                var songName = Path.GetFileNameWithoutExtension(song);
                NativeItem songItem = new(songName)
                {
                    Tag = song
                };
                parentMenu.Add(songItem);

                //Restore menu item state during menu population
                RestoreItemState(songItem);
                
                songItem.Selected += (_, _) =>
                {
                    //Save the reference to the item when it's selected during navigation
                    _selectedSong = songItem;
                };
                
                //Play the song and update the menu when the item is selected
                songItem.Activated += (_, _) =>
                {
                    PlaySelectedItem(songItem, parentMenu);
                };
            }

            //Loop through the sub directories of the current directory
            foreach (var subDir in subDirs)
            {
                var dirName = new DirectoryInfo(subDir).Name; //Get the sub directory name
                NativeMenu subMenu = new(dirName, $"Album {dirName}");
                parentMenu.AddSubMenu(subMenu);
                _pool.Add(subMenu);
                PopulateMenu(subDir, subMenu); //Populate the new submenu
            }
        }
        
        private void PopulateMenu(List<string> songs, NativeMenu parentMenu)
        {
            AddMenuControls(parentMenu, true, false);
            
            parentMenu.Closing += (_, _) =>
            {
                //Delete the saved reference, if any. This will prevent any errors if the menu is closed.
                _selectedSong = null;
                _selectedMenu = null;
                _inPlaylistMenu = false;
            };

            parentMenu.Shown += (_, _) =>
            {
                _inPlaylistMenu = true;
                RestoreItemsState(parentMenu);
            };
            
            //Loop through songs
            foreach (var song in songs)
            {
                if (!File.Exists(song)) continue;
                
                var songName = Path.GetFileNameWithoutExtension(song);
                NativeItem songItem = new(songName)
                {
                    Tag = song
                };
                parentMenu.Add(songItem);

                //Restore menu item state during menu population
                RestoreItemState(songItem);
                
                songItem.Selected += (_, _) =>
                {
                    //Save the reference to the item when it's selected during navigation
                    _selectedSong = songItem;
                };
                
                //Play the song and update the menu when the item is selected
                songItem.Activated += (_, _) =>
                {
                    PlaySelectedItem(songItem, parentMenu);
                };
            }
        }

        public static int GetSkipValue()
        {
            return _skipValue;
        }

        private void NextNotification()
        {
            if (!_showNextNoti) return;
            GTA.UI.Notification.Show($"MusicMenu: Now playing {Music.GetLastPlayedName()}", true);
            _showNextNoti = false;
        }

        private bool PlaylistNameExists(string name) => _playlists.Any(playlist => playlist.PlaylistName == name);

        private void UpdatePlaylist(string name)
        {
            foreach (var playlist in _playlists.Where(playlist => playlist.PlaylistName == name))
            {
                foreach (var song in _playlistSelection.Where(song => !playlist.Songs.Contains(song)))
                {
                    playlist.Songs.Add(song);
                }

                for (var i = playlist.Songs.Count - 1; i > -1; i--)
                {
                    var song = playlist.Songs[i];

                    if (!_playlistSelection.Contains(song))
                        playlist.Songs.Remove(song);
                }
                break;
            }
        }

        private void QueueControls()
        {
            if (_selectedSong is not null)
            {
                if (Function.Call<bool>(Hash.IS_CONTROL_JUST_PRESSED, 0, Main.QueueSong))
                {
                    //Don't add to the queue if it's currently playing
                    if ((string)_selectedSong.Tag != Music.GetLastPlayedPath() || ((string)_selectedSong.Tag == Music.GetLastPlayedPath() && !Music.Playing()))
                    {
                        if (Music.Queue.Contains((string)_selectedSong.Tag))
                        {
                            Music.Queue.Remove((string)_selectedSong.Tag);
                            SetItemState(_selectedSong, ItemState.None);
                            GTA.UI.Notification.Show($"MusicMenu: {Path.GetFileNameWithoutExtension((string)_selectedSong.Tag)} removed from the queue.", true);
                        }
                        else
                        {
                            Music.Queue.Add((string)_selectedSong.Tag);
                            SetItemState(_selectedSong, ItemState.Queued);
                            GTA.UI.Notification.Show($"MusicMenu: {Path.GetFileNameWithoutExtension((string)_selectedSong.Tag)} added to the queue.", true);
                        }
                    }
                }
            }
            
            if (_selectedMenu is not null)
            {
                if (Function.Call<bool>(Hash.IS_CONTROL_JUST_PRESSED, 0, Main.QueueAll))
                {
                    _selectedMenu.Items.ForEach(item =>
                    {
                        if (item.Tag is null) return;
                        if ((string)item.Tag == Music.GetLastPlayedPath() &&
                            ((string)item.Tag != Music.GetLastPlayedPath() || Music.Playing())) return;
                        if (Music.Queue.Contains((string)item.Tag)) return;
                        Music.Queue.Add((string)item.Tag);
                        SetItemState(item, ItemState.Queued);
                    });
                }
            }
        }

        private void SavePlaylist(bool updated, string name)
        {
            try
            {
                var path = $"{Directory.GetCurrentDirectory()}\\scripts\\MusicMenu\\playlists.json";

                if (File.Exists(path)) File.Delete(path);

                File.WriteAllText(path, JsonConvert.SerializeObject(_playlists));
                GTA.UI.Notification.Show($"MusicMenu: playlist {name} {(updated ? "updated" : "saved")}", true);
            }
            catch (Exception)
            {
                GTA.UI.Notification.Show("MusicMenu: Error saving playlists", true);
            }
            finally
            {
                _playlistSelection.Clear();
            }
        }
        
        private void PlaylistControls()
        {
            if (_selectedSong is not null)
            {
                if (Function.Call<bool>(Hash.IS_CONTROL_JUST_PRESSED, 0, Main.AddToPlaylist))
                {
                    if (_playlistSelection.Contains((string)_selectedSong.Tag))
                    {
                        SetItemState(_selectedSong, ItemState.None, BadgeSide.Left);
                        _playlistSelection.Remove((string)_selectedSong.Tag);
                    }
                    else
                    {
                        SetItemState(_selectedSong, ItemState.Selected, BadgeSide.Left);
                        _playlistSelection.Add((string)_selectedSong.Tag);
                    }
                }
            }
            
            if (_selectedMenu is not null)
            {
                if (Function.Call<bool>(Hash.IS_CONTROL_JUST_PRESSED, 0, Main.SavePlaylist))
                {
                    if (_playlistSelection.Count == 0) return;
                    var name = Game.GetUserInput(WindowTitle.EnterMessage20, "My Playlist", 20);
                    if (name.Length == 0) return;

                    if (PlaylistNameExists(name))
                    {
                        UpdatePlaylist(name);
                        SavePlaylist(true, name);
                    }
                    else
                    {
                        _playlists.Add(new()
                        {
                            Songs = _playlistSelection,
                            PlaylistName = name
                        });
                        SavePlaylist(false, name);
                    }
                }
            }
        }
        
        private void ProcessControls()
        {
             QueueControls();
             if (!_inPlaylistMenu) PlaylistControls();
        }

        private void UpdateNextButton()
        {
            switch (Music.Queue.Count)
            {
                case > 0 when !_nextButton.Enabled:
                    _nextButton.Enabled = true;
                    _nextButton.Description = "Play the next song in the queue";
                    break;
                case 0 when _nextButton.Enabled:
                    _nextButton.Enabled = false;
                    _nextButton.Description = "The queue is empty";
                    break;
            }
        }

        private void ProcessUpdateFlag()
        {
            switch (_pool.AreAnyVisible)
            {
                case false when !_update:
                    _update = true; //Reset the variable when all the menus are closed
                    break;
                case true:
                {
                    ProcessControls();
                    break;
                }
            }
        }
        
        public void Process()
        {
            NextNotification();
            ProcessUpdateFlag();
            UpdateNextButton();
            _pool.Process();
        }

        public bool Visible()
        {
            return _pool.AreAnyVisible;
        }

        public static void Open()
        {
            _mainMenu.Visible = true;
        }

        //Returns the current value of the slider
        public static int GetVolume()
        {
            return _volumeSlider.Value;
        }
        
        public Menu()
        {
            _pool = new();
            _mainMenu = new("Music Menu");
            NativeMenu musicMenu = new("Music", "Music");
            NativeMenu settingsMenu = new("Settings", "Settings");
            _stopButton = new("Stop")
            {
                Enabled = false
            };
            _volumeSlider = new("Volume", "Change the volume of the song")
            {
                Maximum = 100,
                Value = 50,
                Description = "Volume: 50"
            };
            NativeListItem<string> scaleSlider =
                new("Time scale", "Set the scale for the time value", "Milliseconds", "Seconds", "Minutes")
                {
                    SelectedIndex = 1
                };
            NativeSliderItem skipValueSlider = new("Time to skip", "Set the amount of time to be skipped.", 100, 1);
            NativeListItem<string> directionSlider = new("Direction", "The direction in which time will be skipped.", "Forward", "Backwards");
            NativeItem skipButton = new("Skip", "Skips the song in the given direction, time and scale");
            NativeCheckboxItem loopToggle = new("Loop", false)
            {
                Description = "If the current song should be repeated indefinitely."
            };
            NativeCheckboxItem progressToggle = new("Playback progress", true)
            {
                Description = "Set whether or not the song playback progress should be displayed."
            };
            NativeItem pauseToggle = new("Pause/Resume", "Pause or Resume the current song");
            NativeItem resetSong = new("Restart", "Play the current song from the beginning");
            NativeItem playRandom = new("Random", "Play a random song");
            NativeCheckboxItem keepPlaying = new("Keep playing", "Keep playing random songs if the queue is empty")
            {
                Checked = false
            };
            NativeMenu playlistsMenu = new("Playlists", "Playlists")
            {
                NoItemsText = "No playlists saved."
            };
            _nextButton = new("Next song", "Play the next song in the queue");
            
            _mainMenu.AddSubMenu(musicMenu);
            _mainMenu.AddSubMenu(playlistsMenu);
            _mainMenu.AddSubMenu(new SpotifyMenu("Spotify", "Spotify", _pool));
            _mainMenu.Add(pauseToggle);
            _mainMenu.Add(_stopButton);
            _mainMenu.Add(_nextButton);
            _mainMenu.Add(resetSong);
            _mainMenu.Add(playRandom);
            _mainMenu.AddSubMenu(settingsMenu);
            settingsMenu.Add(progressToggle);
            settingsMenu.Add(loopToggle);
            settingsMenu.Add(keepPlaying);
            settingsMenu.Add(_volumeSlider);
            settingsMenu.Add(new NativeSeparatorItem());
            settingsMenu.Add(skipValueSlider);
            settingsMenu.Add(scaleSlider);
            settingsMenu.Add(directionSlider);
            settingsMenu.Add(skipButton);
            _pool.Add(_mainMenu);
            _pool.Add(musicMenu);
            _pool.Add(playlistsMenu);
            _pool.Add(settingsMenu);

            musicMenu.Shown += (_, _) =>
            {
                if (!_update) return; //Menu was already populated, don't update
                
                musicMenu.Clear();
                PopulateMenu($"{Directory.GetCurrentDirectory()}\\scripts\\MusicMenu\\Music", musicMenu);
                _update = false;
            };

            playlistsMenu.Shown += (_, _) =>
            {
                _inPlaylistMenu = true;
                playlistsMenu.Clear();
                var data = Playlist.ReadFromDisk();
                if (data == null) return;
                _playlists = data;

                foreach (var playlist in _playlists)
                {
                    NativeMenu playlistMenu = new(playlist.PlaylistName, playlist.PlaylistName)
                    {
                        NoItemsText = "Empty playlist."
                    };
                    PopulateMenu(playlist.Songs, playlistMenu);
                    playlistsMenu.AddSubMenu(playlistMenu);
                    _pool.Add(playlistMenu);
                }
            };

            playlistsMenu.Closing += (_, _) =>
            {
                _inPlaylistMenu = false;
            };

            _stopButton.Activated += (_, _) =>
            {
                //The music stop event was triggered by the player
                _triggeredByPlayer = true; 
                Music.Stop();
                Music.Queue.Clear();
            };

            //Register local event handler that only updates the description
            _volumeSlider.ValueChanged += (_, _) =>
            {
                _volumeSlider.Description = $"Volume: {GetVolume()}";
            };
            //Second event handler in the music class, that actually handles the volume change
            _volumeSlider.ValueChanged += Music.OnVolumeChange;

            progressToggle.Activated += (_, _) =>
            {
                ShowProgress = progressToggle.Checked;
            };

            loopToggle.Activated += (_, _) =>
            {
                _loop = loopToggle.Checked;
            };

            skipValueSlider.ValueChanged += (_, _) =>
            {
                _skipValue = skipValueSlider.Value;
            };

            scaleSlider.ItemChanged += (_, _) =>
            {
                Music.SkipScale = scaleSlider.SelectedItem switch
                {
                    "Milliseconds" => Music.Scale.Milliseconds,
                    "Seconds" => Music.Scale.Seconds,
                    _ => Music.Scale.Minutes
                };
            };

            directionSlider.ItemChanged += (_, _) =>
            {
                Music.SkipDirection = directionSlider.SelectedItem switch
                {
                    "Forward" => Music.Direction.Forward,
                    _ => Music.Direction.Backwards
                };
            };

            pauseToggle.Activated += Music.OnTogglePause;
            resetSong.Activated += Music.OnReset;
            skipButton.Activated += Music.OnSkip;

            _nextButton.Activated += (_, _) =>
            {
                _triggeredByPlayer = true;
                Music.PlaySong(Music.Queue[0]);
                Music.Queue.RemoveAt(0);
                ToggleUi(true);
            };

            playRandom.Activated += (_, _) =>
            {
                PlayRandomSong();
            };

            keepPlaying.Activated += (_, _) =>
            {
                _shouldKeepPlaying = keepPlaying.Checked;
            };
        }
    }
}