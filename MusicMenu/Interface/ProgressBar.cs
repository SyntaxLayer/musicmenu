using System.Drawing;
using System.IO;
using LemonUI;
using LemonUI.TimerBars;
using MusicMenu.Helpers;

namespace MusicMenu.Interface;

public class ProgressBar
{
    private readonly ObjectPool _pool;
    private readonly TimerBarCollection _collection;
    private readonly TimerBarProgress _timerBarProgress;
    private bool _showNextNoti = true;

    public void Process(bool menuVisible)
    {
        _collection.Visible = Music.Playing() && Menu.ShowProgress && !menuVisible;

        if (_collection.Visible)
        {
            if (_timerBarProgress.Title != Music.GetLastPlayedName())
                _timerBarProgress.Title = Music.GetLastPlayedName();
            
            var progress = Music.GetMusicProgress() * 100.0f;

            if (progress is >= 0.0f and <= 100.0f)
            {
                switch (progress)
                {
                    case >= 93.5f when _showNextNoti && Music.Queue.Count > 0:
                        GTA.UI.Notification.Show($"MusicMenu: Up next {Path.GetFileNameWithoutExtension(Music.Queue[0])}", true);
                        _showNextNoti = false;
                        break;
                    case < 93.5f when !_showNextNoti:
                        _showNextNoti = true;
                        break;
                }

                _timerBarProgress.Progress = progress;
            }
        }
        
        _pool.Process();
    }
    
    public ProgressBar()
    {
        _pool = new();
        _collection = new();
        _timerBarProgress = new("Song progress");
        _timerBarProgress.BackgroundColor = Color.Black;
        _timerBarProgress.ForegroundColor = Color.White;
        _collection.Add(_timerBarProgress);
        _pool.Add(_collection);
    }
}