using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using LemonUI;
using LemonUI.Menus;
using MusicMenu.Helpers;
using MusicMenu.Utils;
using MusicMenu.Utils.Types.Spotify;

namespace MusicMenu.Interface;

public class SpotifyMenu : NativeMenu
{
    private readonly ObjectPool _pool;
    private NativeMenu _devicesMenu;
    private NativeItem _refreshButton;
    private NativeItem _playButton;
    private NativeItem _pauseButton;
    private NativeListItem<string> _skipOption, _repeatModeOption;
    private NativeListItem<bool> _shuffleToggle;
    private NativeListItem<int> _volumeOption;
    private NativeItem _restartOption;
    private List<Device> _devices = new();
    private static bool DevicesUpdated { get; set; }
    private static bool TokenUpdated { get; set; }
    private static bool PauseMessage { get; set; }
    private static bool PlayMessage { get; set; }
    private static bool TransferMessage { get; set; }
    public static string RequestMessage { get; set; } = "";
    private static bool SkipMessage { get; set; }
    private static bool RestartMessage { get; set; }
    private static bool RepeatMessage { get; set; }
    private static bool ShuffleMessage { get; set; }
    private static bool VolumeMessage { get; set; }
    
    
    private void AddSubmenus()
    {
        _devicesMenu = new("Devices", "Devices");
        _refreshButton = new("Refresh token",
            "Select to request a new Spotify access token. Will end in error if refresh token is not set.");
        _pauseButton = new("Pause", "Press to pause the playback");
        _playButton = new("Resume", "Press to resume the playback");
        _skipOption = new("Skip track", new[] { "Next", "Previous" });
        _restartOption = new("Restart track");
        _repeatModeOption = new("Playback repeat mode", new[] { "Track", "Context", "Off" });
        _shuffleToggle = new("Playback shuffle", true, false);
        _volumeOption = new("Playback volume", Enumerable.Range(0, 100).ToArray());

        _devicesMenu.Opening += DevicesMenuOnOpening;
        _refreshButton.Activated += RefreshButtonOnActivated;
        _playButton.Activated += PlayButtonOnActivated;
        _pauseButton.Activated += PauseButtonOnActivated;
        _skipOption.Activated += SkipOptionOnActivated;
        _restartOption.Activated += RestartOptionOnActivated;
        _repeatModeOption.Activated += RepeatModeOptionOnActivated;
        _shuffleToggle.Activated += ShuffleToggleOnActivated;
        _volumeOption.Activated += VolumeOptionOnActivated;

        Add(_refreshButton);
        Add(_playButton);
        Add(_pauseButton);
        Add(_skipOption);
        Add(_restartOption);
        Add(_repeatModeOption);
        Add(_shuffleToggle);
        Add(_volumeOption);
        AddSubMenu(_devicesMenu);
        AddSubMenu(new CollectionList("Playlists", "Playlists", 0, CollectionListType.Playlists, _pool));
        AddSubMenu(new CollectionList("Saved Albums", "Saved Albums", 0, CollectionListType.SavedAlbums, _pool));
        AddSubMenu(new TrackCollection("Saved tracks", 0, CollectionType.SavedTracks, _pool));
        _pool.Add(_devicesMenu);
    }

    private async void RunSetVolume(int volume)
    {
        await RequestHandler.SendVolume(volume);
        VolumeMessage = true;
    }
    
    private void VolumeOptionOnActivated(object sender, EventArgs e)
    {
        GTA.UI.Notification.Show("Changing volume...", true);
        RunSetVolume(_volumeOption.SelectedItem);
    }

    private async void RunShuffleToggle(bool on)
    {
        await RequestHandler.SendShuffle(on);
        ShuffleMessage = true;
    }
    
    private void ShuffleToggleOnActivated(object sender, EventArgs e)
    {
        var on = _shuffleToggle.SelectedItem;
        GTA.UI.Notification.Show($"{(on ? "Activating" : "Deactivating")} shuffle mode...", true);
        RunShuffleToggle(on);
    }

    private async void RunRepeatMode(string mode)
    {
        await RequestHandler.SendRepeatMode(mode);
        RepeatMessage = true;
    }
    
    private void RepeatModeOptionOnActivated(object sender, EventArgs e)
    {
        GTA.UI.Notification.Show($"Setting repeat mode to {_repeatModeOption.SelectedItem.ToLower()}...", true);
        RunRepeatMode(_repeatModeOption.SelectedItem.ToLower());
    }

    private async void RunRestart()
    {
        await RequestHandler.SendSeek();
        RestartMessage = true;
    }
    
    private void RestartOptionOnActivated(object sender, EventArgs e)
    {
        GTA.UI.Notification.Show("Restarting track...", true);
        RunRestart();
    }

    private async void RunSkip(string direction)
    {
        await RequestHandler.SendSkip(direction.ToLower());
        SkipMessage = true;
    }
    
    private void SkipOptionOnActivated(object sender, EventArgs e)
    {
        GTA.UI.Notification.Show("Skipping...", true);
        RunSkip(_skipOption.SelectedItem);
    }

    private async void RunDevicesFetch()
    {
        var result = await RequestHandler.GetDevices();

        if (result == null) _devices.Clear();
        else _devices = result.UserDevices;

        DevicesUpdated = true;
    }

    private void DevicesMenuOnOpening(object sender, CancelEventArgs e)
    {
        GTA.UI.Notification.Show("Fetching devices...", true);
        RunDevicesFetch();
    }

    private static async void RunPause()
    {
        await RequestHandler.SendPause();
        PauseMessage = true;
    }
    
    private void PauseButtonOnActivated(object sender, EventArgs e)
    {
        _playButton.Enabled = false;
        _pauseButton.Enabled = false;
        RunPause();
        GTA.UI.Notification.Show("Pausing playback...", true);
    }

    private static async void RunPlay()
    {
        await RequestHandler.SendPlay();
        PlayMessage = true;
    }
    
    private void PlayButtonOnActivated(object sender, EventArgs e)
    {
        _playButton.Enabled = false;
        _pauseButton.Enabled = false;
        RunPlay();
        GTA.UI.Notification.Show("Resuming playback...", true);
    }

    private async void RunGetToken()
    {
        var response = await RequestHandler.GetToken();

        if (response?.Token != null)
        {
            Tokens.AccessToken = response.Token;
            ScriptConfig.UpdateFile();
        }
        
        TokenUpdated = true;
    }

    private async void RunTransfer(string deviceId)
    {
        await RequestHandler.SendTransfer(deviceId);
        TransferMessage = true;
    }
    
    private void RefreshButtonOnActivated(object sender, EventArgs e)
    {
        RunGetToken();
        GTA.UI.Notification.Show("Refreshing token...", true);
    }

    private void DevicesMenuUpdate()
    {
        GTA.UI.Notification.Show($"MusicMenu: {RequestMessage}");
        _devicesMenu.Clear();
        
        foreach (var device in _devices)
        {
            NativeCheckboxItem deviceCheckbox = new(device.Name, device.IsActive)
            {
                Tag = device.Id
            };

            deviceCheckbox.Activated += (_, _) =>
            {
                if (!deviceCheckbox.Checked)
                {
                    RunPause();
                }
                else
                {
                    _devicesMenu.Items.ForEach(item =>
                    {
                        if ((string)item.Tag != (string)deviceCheckbox.Tag && ((NativeCheckboxItem)item).Checked)
                        {
                            ((NativeCheckboxItem)item).Checked = false;
                        }
                    });
                    RunTransfer((string)deviceCheckbox.Tag);
                }
            };
            _devicesMenu.Add(deviceCheckbox);
        }
    }

    private void ShowRequestResponse()
    {
        if (DevicesUpdated)
        {
            DevicesMenuUpdate();
            DevicesUpdated = false;
        }

        if (TokenUpdated)
        {
            GTA.UI.Notification.Show(RequestMessage, true);
            TokenUpdated = false;
        }

        if (PlayMessage)
        {
            _playButton.Enabled = true;
            _pauseButton.Enabled = true;
            GTA.UI.Notification.Show(RequestMessage, true);
            PlayMessage = false;
        }

        if (PauseMessage)
        {
            _pauseButton.Enabled = true;
            _playButton.Enabled = true;
            GTA.UI.Notification.Show(RequestMessage, true);
            PauseMessage = false;
        }

        if (TransferMessage)
        {
            GTA.UI.Notification.Show(RequestMessage, true);
            TransferMessage = false;
        }

        if (SkipMessage)
        {
            GTA.UI.Notification.Show(RequestMessage, true);
            SkipMessage = false;
        }

        if (RestartMessage)
        {
            GTA.UI.Notification.Show(RequestMessage, true);
            RestartMessage = false;
        }
        
        if (RepeatMessage)
        {
            GTA.UI.Notification.Show(RequestMessage, true);
            RepeatMessage = false;
        }
        
        if (ShuffleMessage)
        {
            GTA.UI.Notification.Show(RequestMessage, true);
            ShuffleMessage = false;
        }
        
        if (VolumeMessage)
        {
            GTA.UI.Notification.Show(RequestMessage, true);
            VolumeMessage = false;
        }
    }
    
    public override void Process()
    {
        ShowRequestResponse();
        base.Process();
    }

    public SpotifyMenu(string title, string subtitle, ObjectPool pool) : base(title, subtitle)
    {
        _pool = pool;
        NoItemsText = "Access token not found.";
        AddSubmenus();

        Shown += (_, _) =>
        {
            if (Tokens.AccessToken == null) Clear();
            else if (Items.Count == 0) AddSubmenus();
        };
        
        _pool.Add(this);
    }
}