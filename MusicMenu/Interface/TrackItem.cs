using LemonUI.Menus;

namespace MusicMenu.Interface;

public class TrackItem : NativeItem
{
    private string _id;
    private string _uri;
    public static string RequestMessage { get; set; }
    public static bool ShowMessage { private get; set; }

    public TrackItem(string title, string id, string uri) : base(title, title)
    {
        _id = id;
        _uri = uri;
    }
}