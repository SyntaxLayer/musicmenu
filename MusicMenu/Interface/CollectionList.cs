using System.Collections.Generic;
using System.ComponentModel;
using GTA;
using GTA.Native;
using LemonUI;
using LemonUI.Menus;
using MusicMenu.Helpers;
using MusicMenu.Utils.Types.Requests;

namespace MusicMenu.Interface;

public enum CollectionListType
{
    Playlists,
    SavedAlbums
}

public class CollectionList : NativeMenu
{
    private readonly CollectionListType _type;
    private List<SpotifyItem> _items = new();
    private readonly int _offset;
    private bool _next;
    private bool _listUpdated;
    private bool _playbackRequest;
    public static string RequestMessage { get; set; } = "";
    private readonly ObjectPool _pool;

    public CollectionList(string title, string itemTitle, int offset, CollectionListType type, ObjectPool pool) : base(title, title)
    {
        _type = type;
        _offset = offset;
        _pool = pool;
        _pool.Add(this);
        AddMenuControls();

        if (offset > 0)
        {
            Title.Text = itemTitle;
            Subtitle = itemTitle;

            Opening += (_, _) =>
            {
                Title.Text = title;
                Subtitle = $"Page: {(_offset / 50) + 1}";
            };

            Closing += (_, _) =>
            {
                Title.Text = itemTitle;
                Subtitle = itemTitle;
            };
        }
        
        Opening += OnOpening;
    }

    private async void RunFetch()
    {
        var response = _type switch
        {
            CollectionListType.Playlists => await RequestHandler.FetchPlaylists(_offset),
            _ => await RequestHandler.FetchAlbums(_offset)
        };

        if (response == null)
        {
            _items.Clear();
            _next = false;
        }
        else
        {
            _items = response.Items;
            _next = response.Next;
        }

        _listUpdated = true;
    }
    
    private void AddMenuControls()
    {
        if (RequiredControls.Contains((Control)Main.QueueSong)) return;
        
        Buttons.Add(new("Play", (Control)Main.QueueSong));
        RequiredControls.Add((Control)Main.QueueSong);
    }
    
    private void OnOpening(object sender, CancelEventArgs e)
    {
        GTA.UI.Notification.Show($"Fetching {(_type == CollectionListType.Playlists ? "playlists" : "albums")}...");
        RunFetch();
    }

    private void UpdateList()
    {
        GTA.UI.Notification.Show(RequestMessage, true);
        Clear();

        foreach (var item in _items)
        {
            AddSubMenu(new TrackCollection(item, 0, _type, _pool));
        }

        if (_next)
        {
            var title = _type == CollectionListType.Playlists ? "Playlists" : "Saved albums";
            AddSubMenu(new CollectionList(title, "Next", _offset + 50, _type, _pool));
        }
    }

    private async void RunPlay(string uri)
    {
        await RequestHandler.SendPlay(uri, null);
        _playbackRequest = true;
    }
    
    public override void Process()
    {
        if (_listUpdated)
        {
            UpdateList();
            _listUpdated = false;
        }

        if (_items.Count > 0)
        {
            if (_playbackRequest)
            {
                GTA.UI.Notification.Show(RequestMessage, true);
                _playbackRequest = false;
            }

            if (Function.Call<bool>(Hash.IS_CONTROL_JUST_PRESSED, 0, Main.QueueSong) && Visible)
            {
                var item = _items[SelectedIndex];
                GTA.UI.Notification.Show($"Playing {item.Name}...");
                RunPlay(item.Uri);
            }
        }
        
        base.Process();
    }
}