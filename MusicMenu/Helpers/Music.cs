using System;
using System.Collections.Generic;
using System.IO;
using MusicMenu.Interface;
using NAudio.Wave;
using NAudio.Wave.SampleProviders;

namespace MusicMenu.Helpers;

public class Music
{
    public enum Scale
    {
        Milliseconds,
        Seconds,
        Minutes
    }

    public enum Direction
    {
        Forward,
        Backwards
    }
    
    private static WaveOutEvent? _waveOut;
    private static Mp3FileReader? _mp3Reader;
    private static SampleChannel? _sampleChannel;
    private static float _musicVolume = 0.5f;
    private static string _lastPlayed;
    public static List<string> Queue { get; } = new();
    public static Scale SkipScale { get; set; } = Scale.Seconds;
    public static Direction SkipDirection { get; set; } = Direction.Forward;
    /*private static bool _firstTime = true;
    private static float _startVolume;*/

    //Plays the song file found in the given path
    public static void PlaySong(string path)
    {
        //If a song is playing, stop it.
        if (Playing())
        {
            Stop();
        }
        
        _waveOut = new();
        _mp3Reader = new(path);
        _sampleChannel = new(_mp3Reader);
        _waveOut.Init(_sampleChannel);
        
        /*if (_firstTime)
        {
            _startVolume = _waveOut.Volume;
            _firstTime = false;
        }*/
        
        _sampleChannel.Volume = _musicVolume; //Start the song with the saved volume value
        _waveOut.Play();
        _lastPlayed = path;
        //Crashes the game
        //GTA.UI.Notification.Show($"MusicMenu: Now playing {GetLastPlayedName()}");
        
        _waveOut.PlaybackStopped += WaveOutOnPlaybackStopped;
        _waveOut.PlaybackStopped += Menu.WaveOutOnPlaybackStopped; //Register a second event handle in the menu class
    }
    
    //Triggered from the Menu class by the volume slider
    public static void OnVolumeChange(object sender, EventArgs e)
    {
        //Get the value from the slider and turn it into a float that goes from 0.0 to 1.0
        _musicVolume = Menu.GetVolume() / 100.0f;

        //Update the volume if a song is playing
        if (Playing())
        {
            _sampleChannel!.Volume = _musicVolume;
        }
    }

    public static void OnReset(object sender, EventArgs e)
    {
        if (Playing())
        {
            _mp3Reader!.CurrentTime = TimeSpan.Zero;
        }
    }

    public static void OnSkip(object sender, EventArgs e)
    {
        if (!Playing()) return;
        
        var time = SkipScale switch
        {
            Scale.Milliseconds => TimeSpan.FromMilliseconds(Menu.GetSkipValue()),
            Scale.Seconds => TimeSpan.FromSeconds(Menu.GetSkipValue()),
            _ => TimeSpan.FromMinutes(Menu.GetSkipValue())
        };

        if (SkipDirection == Direction.Forward)
        {
            if (time < _mp3Reader!.TotalTime && time + _mp3Reader.CurrentTime < _mp3Reader.TotalTime)
                _mp3Reader.CurrentTime += time;
            else
                _mp3Reader.CurrentTime = _mp3Reader.TotalTime;
        }
        else
        {
            if (time == TimeSpan.Zero) return;

            if (_mp3Reader!.CurrentTime - time >= TimeSpan.Zero)
                _mp3Reader.CurrentTime -= time;
            else
                _mp3Reader.CurrentTime = TimeSpan.Zero;
        }
    }
    
    public static void OnTogglePause(object sender, EventArgs e)
    {
        if (!Playing()) return;
        
        if (_waveOut!.PlaybackState is PlaybackState.Paused)
            _waveOut.Play();
        else
            _waveOut.Pause();
    }
    
    public static string GetLastPlayedPath()
    {
        return _lastPlayed;
    }

    public static string GetLastPlayedName()
    {
        return Path.GetFileNameWithoutExtension(_lastPlayed);
    }
    
    public static float GetMusicProgress()
    {
        return _mp3Reader is null ? 
            0.0f 
            : 
            (float)(_mp3Reader.CurrentTime.TotalMilliseconds / _mp3Reader.TotalTime.TotalMilliseconds);
    }
    
    public static bool Playing()
    {
        return _waveOut is { PlaybackState: PlaybackState.Playing or PlaybackState.Paused };
    }

    public static bool IsPaused()
    {
        return _waveOut is { PlaybackState: PlaybackState.Paused };
    }

    public static void Pause()
    {
        _waveOut!.Pause();
    }

    public static void Resume()
    {
        _waveOut!.Play();
    }
    
    public static bool IsPlaying()
    {
        return _waveOut is { PlaybackState: PlaybackState.Playing };
    }
    
    private static void WaveOutOnPlaybackStopped(object sender, StoppedEventArgs e)
    {
        _mp3Reader?.Dispose();
        _waveOut?.Dispose();
    }

    public static void Stop()
    {
        _waveOut?.Stop();
    }
}