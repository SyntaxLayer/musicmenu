using System;
using System.IO;
using System.Net;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using MusicMenu.Interface;
using MusicMenu.Utils;
using MusicMenu.Utils.Types.Requests;
using MusicMenu.Utils.Types.Spotify;
using Newtonsoft.Json;

namespace MusicMenu.Helpers;

using System.Net.Http;

public static class RequestHandler
{
    private static readonly HttpClient Client = new();

    public static async Task<Devices?> GetDevices()
    {
        if (Tokens.AccessToken is null or "")
        {
            SpotifyMenu.RequestMessage = "Access token not set.";
            return null;
        }
        
        try
        {
            Client.DefaultRequestHeaders.Authorization = 
            AuthenticationHeaderValue.Parse($"Bearer {Tokens.AccessToken}");
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 |
                                                              SecurityProtocolType.Tls12;
            var response = await Client.GetAsync("https://music-menu.herokuapp.com/devices");

            if (response.IsSuccessStatusCode)
            {
                var devices = JsonConvert.DeserializeObject<Devices>(await response.Content.ReadAsStringAsync());
                SpotifyMenu.RequestMessage = devices?.UserDevices.Count == 0 ? "No active devices" : "Request completed.";
                return devices;
            }

            var msg = JsonConvert.DeserializeObject<Message>(await response.Content.ReadAsStringAsync());
            SpotifyMenu.RequestMessage = msg?.Text ?? "Error fetching devices";
            return null;

        }
        catch (Exception e)
        {
            File.AppendAllLines("mmlog.txt", new []{ e.Message, e.StackTrace, e.InnerException?.Message, e.InnerException?.StackTrace });
            SpotifyMenu.RequestMessage = "Error fetching devices...";
            return null;
        }
        finally
        {
            File.AppendAllLines("mmlog.txt", new []{ SpotifyMenu.RequestMessage });
        }
    }
    
    public static async Task<Message?> GetToken()
    {
        if (Tokens.RefreshToken is null or "")
        {
            SpotifyMenu.RequestMessage = "Refresh token not registered.";
            return null;
        }

        SpotifyMenu.RequestMessage = "Requesting new token...";
        try
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 |
                                                   SecurityProtocolType.Tls12;
            var response = await Client.PutAsync(
                "https://music-menu.herokuapp.com/refresh",
                new StringContent(
                    string.Concat("{\"refreshToken\": \"", Tokens.RefreshToken, "\"}"),
                    Encoding.UTF8,
                    "application/json"));
            var message = JsonConvert.DeserializeObject<Message>(await response.Content.ReadAsStringAsync());
            SpotifyMenu.RequestMessage = message!.Text;
            return response.IsSuccessStatusCode ? message : null;
        }
        catch (Exception e)
        {
            File.AppendAllLines("mmlog.txt", new []{ e.Message, e.StackTrace, e.InnerException?.Message, e.InnerException?.StackTrace });
            SpotifyMenu.RequestMessage= "Error requesting a new token...";
            return null;
        }
        finally
        {
            File.AppendAllLines("mmlog.txt", new []{ SpotifyMenu.RequestMessage });
        }
    }
    
    public static async Task SendPause()
    {
        if (Tokens.AccessToken is null or "")
        {
            SpotifyMenu.RequestMessage = "Refresh token not registered.";
            return;
        }

        SpotifyMenu.RequestMessage = "Pausing playback...";
        try
        {
            Client.DefaultRequestHeaders.Authorization = 
                AuthenticationHeaderValue.Parse($"Bearer {Tokens.AccessToken}");
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 |
                                                   SecurityProtocolType.Tls12;
            var response = await Client.PutAsync(
                "https://music-menu.herokuapp.com/pause",
                new StringContent("{}", Encoding.UTF8, "application/json"));
            var message = JsonConvert.DeserializeObject<Message>(await response.Content.ReadAsStringAsync());
            SpotifyMenu.RequestMessage = message!.Text;
        }
        catch (Exception e)
        {
            File.AppendAllLines("mmlog.txt", new []{ e.Message, e.StackTrace, e.InnerException?.Message, e.InnerException?.StackTrace });
            SpotifyMenu.RequestMessage = "Error pausing playback...";
        }
        finally
        {
            File.AppendAllLines("mmlog.txt", new []{ SpotifyMenu.RequestMessage });
        }
    }
    
    public static async Task SendPlay()
    {
        if (Tokens.AccessToken is null or "")
        {
            SpotifyMenu.RequestMessage = "Refresh token not registered.";
            return;
        }

        SpotifyMenu.RequestMessage = "Resuming playback...";
        try
        {
            Client.DefaultRequestHeaders.Authorization = 
                AuthenticationHeaderValue.Parse($"Bearer {Tokens.AccessToken}");
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 |
                                                   SecurityProtocolType.Tls12;
            var response = await Client.PutAsync(
                "https://music-menu.herokuapp.com/play",
                new StringContent("{}", Encoding.UTF8, "application/json"));
            var message = JsonConvert.DeserializeObject<Message>(await response.Content.ReadAsStringAsync());
            SpotifyMenu.RequestMessage = message!.Text;
        }
        catch (Exception e)
        {
            File.AppendAllLines("mmlog.txt", new []{ e.Message, e.StackTrace, e.InnerException?.Message, e.InnerException?.StackTrace });
            SpotifyMenu.RequestMessage = "Error resuming playback";
        }
        finally
        {
            File.AppendAllLines("mmlog.txt", new []{ SpotifyMenu.RequestMessage });
        }
    }
    
    public static async Task SendPlay(string? playlistUri, string? trackUri)
    {
        if (Tokens.AccessToken is null or "")
        {
            CollectionList.RequestMessage = "Refresh token not registered.";
            TrackCollection.RequestMessage = "Refresh token not registered.";
            return;
        }

        if (playlistUri == null && trackUri == null)
        {
            CollectionList.RequestMessage = "At least one context uri type must be specified.";
            TrackCollection.RequestMessage = "At least one context uri type must be specified.";
            return;
        }

        CollectionList.RequestMessage = "Playing...";
        TrackCollection.RequestMessage = "Playing...";
        try
        {
            var requestBody = "";

            if (playlistUri != null)
            {
                requestBody = string.Concat("{\"context_uri\": \"", playlistUri, "\"}");
            }
            else if (trackUri != null)
            {
                requestBody = string.Concat("{\"uris\": [\"", trackUri, "\"]}");
            }
            
            Client.DefaultRequestHeaders.Authorization = 
                AuthenticationHeaderValue.Parse($"Bearer {Tokens.AccessToken}");
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 |
                                                   SecurityProtocolType.Tls12;
            var response = await Client.PutAsync(
                "https://music-menu.herokuapp.com/play",
                new StringContent(requestBody, Encoding.UTF8,
                    "application/json"));
            var message = JsonConvert.DeserializeObject<Message>(await response.Content.ReadAsStringAsync());
            CollectionList.RequestMessage = message!.Text;
            TrackCollection.RequestMessage = message.Text;
        }
        catch (Exception e)
        {
            File.AppendAllLines("mmlog.txt", new []{ e.Message, e.StackTrace, e.InnerException?.Message, e.InnerException?.StackTrace });
            CollectionList.RequestMessage = "Error starting playback.";
            TrackCollection.RequestMessage = "Error starting playback.";
        }
        finally
        {
            File.AppendAllLines("mmlog.txt", new []{ CollectionList.RequestMessage });
        }
    }

    public static async Task SendQueue(string trackUri)
    {
        if (Tokens.AccessToken is null or "")
        {
            TrackCollection.RequestMessage = "Refresh token not registered.";
            return;
        }

        TrackCollection.RequestMessage = "Queueing...";
        try
        {
            Client.DefaultRequestHeaders.Authorization = 
                AuthenticationHeaderValue.Parse($"Bearer {Tokens.AccessToken}");
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 |
                                                   SecurityProtocolType.Tls12;
            var response = await Client.PostAsync(
                "https://music-menu.herokuapp.com/queue",
                new StringContent(string.Concat("{\"uri\": \"", trackUri, "\"}"), Encoding.UTF8,
                    "application/json"));
            var message = JsonConvert.DeserializeObject<Message>(await response.Content.ReadAsStringAsync());
            TrackCollection.RequestMessage = message!.Text;
        }
        catch (Exception e)
        {
            File.AppendAllLines("mmlog.txt", new []{ e.Message, e.StackTrace, e.InnerException?.Message, e.InnerException?.StackTrace });
            TrackCollection.RequestMessage = "Error adding to queue";
        }
        finally
        {
            File.AppendAllLines("mmlog.txt", new []{ TrackCollection.RequestMessage });
        }
    }
    
    public static async Task SendTransfer(string deviceId)
    {
        if (Tokens.AccessToken is null or "")
        {
            SpotifyMenu.RequestMessage = "Access token not registered.";
            return;
        }

        SpotifyMenu.RequestMessage = "Transferring playback...";
        try
        {
            Client.DefaultRequestHeaders.Authorization = 
                AuthenticationHeaderValue.Parse($"Bearer {Tokens.AccessToken}");
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 |
                                                   SecurityProtocolType.Tls12;
            var response = await Client.PutAsync(
                "https://music-menu.herokuapp.com/transfer",
                new StringContent(
                    string.Concat("{\"deviceId\": \"", deviceId, "\"}"),
                    Encoding.UTF8,
                    "application/json"));
            var message = JsonConvert.DeserializeObject<Message>(await response.Content.ReadAsStringAsync());
            SpotifyMenu.RequestMessage = message!.Text;
        }
        catch (Exception e)
        {
            File.AppendAllLines("mmlog.txt", new []{ e.Message, e.StackTrace, e.InnerException?.Message, e.InnerException?.StackTrace });
            SpotifyMenu.RequestMessage = "Error transferring playback...";
        }
        finally
        {
            File.AppendAllLines("mmlog.txt", new []{ SpotifyMenu.RequestMessage });
        }
    }
    
    public static async Task<ItemPage?> FetchPlaylistTracks(string collectionId, int offset)
    {
        if (Tokens.AccessToken is null or "")
        {
            TrackCollection.RequestMessage = "Access token not registered.";
            return null;
        }

        TrackCollection.RequestMessage = "Fetching tracks...";
        try
        {
            Client.DefaultRequestHeaders.Authorization = 
                AuthenticationHeaderValue.Parse($"Bearer {Tokens.AccessToken}");
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 |
                                                   SecurityProtocolType.Tls12;
            var response = await Client.GetAsync(
                $"https://music-menu.herokuapp.com/playlists/{collectionId}/{offset}/tracks");
            
            if (response.IsSuccessStatusCode)
            {
                var items = JsonConvert.DeserializeObject<ItemPage>(await response.Content.ReadAsStringAsync());
                TrackCollection.RequestMessage = items?.Items.Count == 0 ? "No tracks in this collection" : "Track list updated.";
                return items;
            }

            var msg = JsonConvert.DeserializeObject<Message>(await response.Content.ReadAsStringAsync());
            TrackCollection.RequestMessage = msg?.Text ?? "Error fetching tracks";
            return null;
        }
        catch (Exception e)
        {
            File.AppendAllLines("mmlog.txt", new []{ e.Message, e.StackTrace, e.InnerException?.Message, e.InnerException?.StackTrace });
            TrackCollection.RequestMessage = "Error fetching tracks...";
            return null;
        }
        finally
        {
            File.AppendAllLines("mmlog.txt", new []{ TrackCollection.RequestMessage });
        }
    }
    
    public static async Task<ItemPage?> FetchPlaylists(int offset)
    {
        if (Tokens.AccessToken is null or "")
        {
            CollectionList.RequestMessage = "Access token not registered.";
            return null;
        }

        CollectionList.RequestMessage = "Fetching playlists...";
        try
        {
            Client.DefaultRequestHeaders.Authorization = 
                AuthenticationHeaderValue.Parse($"Bearer {Tokens.AccessToken}");
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 |
                                                   SecurityProtocolType.Tls12;
            var response = await Client.GetAsync($"https://music-menu.herokuapp.com/playlists/{offset}");
            
            if (response.IsSuccessStatusCode)
            {
                var items = JsonConvert.DeserializeObject<ItemPage>(await response.Content.ReadAsStringAsync());
                CollectionList.RequestMessage = items?.Items.Count == 0 ? "No saved playlists" : "Playlist list updated.";
                return items;
            }

            var msg = JsonConvert.DeserializeObject<Message>(await response.Content.ReadAsStringAsync());
            CollectionList.RequestMessage = msg?.Text ?? "Error fetching playlists";
            return null;
        }
        catch (Exception e)
        {
            File.AppendAllLines("mmlog.txt", new []{ e.Message, e.StackTrace, e.InnerException?.Message, e.InnerException?.StackTrace });
            CollectionList.RequestMessage = "Error fetching playlists...";
            return null;
        }
        finally
        {
            File.AppendAllLines("mmlog.txt", new []{ CollectionList.RequestMessage });
        }
    }
    
    public static async Task<ItemPage?> FetchAlbums(int offset)
    {
        if (Tokens.AccessToken is null or "")
        {
            CollectionList.RequestMessage = "Access token not registered.";
            return null;
        }

        CollectionList.RequestMessage = "Fetching saved albums...";
        try
        {
            Client.DefaultRequestHeaders.Authorization = 
                AuthenticationHeaderValue.Parse($"Bearer {Tokens.AccessToken}");
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 |
                                                   SecurityProtocolType.Tls12;
            var response = await Client.GetAsync($"https://music-menu.herokuapp.com/albums/{offset}");
            
            if (response.IsSuccessStatusCode)
            {
                var items = JsonConvert.DeserializeObject<ItemPage>(await response.Content.ReadAsStringAsync());
                CollectionList.RequestMessage = items?.Items.Count == 0 ? "No saved albums" : "Album list updated.";
                return items;
            }

            var msg = JsonConvert.DeserializeObject<Message>(await response.Content.ReadAsStringAsync());
            CollectionList.RequestMessage = msg?.Text ?? "Error fetching saved albums";
            return null;
        }
        catch (Exception e)
        {
            File.AppendAllLines("mmlog.txt", new []{ e.Message, e.StackTrace, e.InnerException?.Message, e.InnerException?.StackTrace });
            CollectionList.RequestMessage = "Error fetching saved albums...";
            return null;
        }
        finally
        {
            File.AppendAllLines("mmlog.txt", new []{ CollectionList.RequestMessage });
        }
    }
    
    public static async Task<ItemPage?> FetchAlbumTracks(string collectionId, int offset)
    {
        if (Tokens.AccessToken is null or "")
        {
            TrackCollection.RequestMessage = "Access token not registered.";
            return null;
        }

        TrackCollection.RequestMessage = "Fetching tracks...";
        try
        {
            Client.DefaultRequestHeaders.Authorization = 
                AuthenticationHeaderValue.Parse($"Bearer {Tokens.AccessToken}");
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 |
                                                   SecurityProtocolType.Tls12;
            var response = await Client.GetAsync(
                $"https://music-menu.herokuapp.com/albums/{collectionId}/{offset}/tracks");
            
            if (response.IsSuccessStatusCode)
            {
                var items = JsonConvert.DeserializeObject<ItemPage>(await response.Content.ReadAsStringAsync());
                TrackCollection.RequestMessage = items?.Items.Count == 0 ? "No tracks in this collection" : "Track list updated.";
                return items;
            }

            var msg = JsonConvert.DeserializeObject<Message>(await response.Content.ReadAsStringAsync());
            TrackCollection.RequestMessage = msg?.Text ?? "Error fetching tracks";
            return null;
        }
        catch (Exception e)
        {
            File.AppendAllLines("mmlog.txt", new []{ e.Message, e.StackTrace, e.InnerException?.Message, e.InnerException?.StackTrace });
            TrackCollection.RequestMessage = "Error fetching tracks...";
            return null;
        }
        finally
        {
            File.AppendAllLines("mmlog.txt", new []{ TrackCollection.RequestMessage });
        }
    }
    
    public static async Task<ItemPage?> FetchTracks(int offset)
    {
        if (Tokens.AccessToken is null or "")
        {
            TrackCollection.RequestMessage = "Access token not registered.";
            return null;
        }

        TrackCollection.RequestMessage = "Fetching saved tracks...";
        try
        {
            Client.DefaultRequestHeaders.Authorization = 
                AuthenticationHeaderValue.Parse($"Bearer {Tokens.AccessToken}");
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 |
                                                   SecurityProtocolType.Tls12;
            var response = await Client.GetAsync($"https://music-menu.herokuapp.com/tracks/{offset}");
            
            if (response.IsSuccessStatusCode)
            {
                var items = JsonConvert.DeserializeObject<ItemPage>(await response.Content.ReadAsStringAsync());
                TrackCollection.RequestMessage = items?.Items.Count == 0 ? "No saved tracks" : "Track list updated.";
                return items;
            }

            var msg = JsonConvert.DeserializeObject<Message>(await response.Content.ReadAsStringAsync());
            TrackCollection.RequestMessage = msg?.Text ?? "Error fetching saved tracks";
            return null;
        }
        catch (Exception e)
        {
            File.AppendAllLines("mmlog.txt", new []{ e.Message, e.StackTrace, e.InnerException?.Message, e.InnerException?.StackTrace });
            TrackCollection.RequestMessage = "Error fetching saved tracks...";
            return null;
        }
        finally
        {
            File.AppendAllLines("mmlog.txt", new []{  TrackCollection.RequestMessage });
        }
    }
    
    public static async Task SendSkip(string direction)
    {
        if (Tokens.AccessToken is null or "")
        {
            SpotifyMenu.RequestMessage = "Refresh token not registered.";
            return;
        }

        if (direction != "next" && direction != "previous")
        {
            SpotifyMenu.RequestMessage = "Direction must be either next or previous.";
            return;
        }

        SpotifyMenu.RequestMessage = $"Skipping {direction}...";
        try
        {
            var requestBody = string.Concat("{\"direction\": \"", direction, "\"}");
            Client.DefaultRequestHeaders.Authorization = 
                AuthenticationHeaderValue.Parse($"Bearer {Tokens.AccessToken}");
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 |
                                                   SecurityProtocolType.Tls12;
            var response = await Client.PostAsync(
                "https://music-menu.herokuapp.com/skip",
                new StringContent(requestBody, Encoding.UTF8,
                    "application/json"));
            var message = JsonConvert.DeserializeObject<Message>(await response.Content.ReadAsStringAsync());
            SpotifyMenu.RequestMessage = message!.Text;
        }
        catch (Exception e)
        {
            File.AppendAllLines("mmlog.txt", new []{ e.Message, e.StackTrace, e.InnerException?.Message, e.InnerException?.StackTrace });
            SpotifyMenu.RequestMessage = "Error skipping song.";
        }
        finally
        {
            File.AppendAllLines("mmlog.txt", new []{ SpotifyMenu.RequestMessage });
        }
    }
    
    public static async Task SendSeek(int position = 0)
    {
        if (Tokens.AccessToken is null or "")
        {
            SpotifyMenu.RequestMessage = "Refresh token not registered.";
            return;
        }

        if (position < 0)
        {
            SpotifyMenu.RequestMessage = "Position must be a positive number";
            return;
        }

        SpotifyMenu.RequestMessage = "Restarting track...";
        try
        {
            var requestBody = string.Concat("{\"position_ms\": \"", position, "\"}");
            Client.DefaultRequestHeaders.Authorization = 
                AuthenticationHeaderValue.Parse($"Bearer {Tokens.AccessToken}");
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 |
                                                   SecurityProtocolType.Tls12;
            var response = await Client.PutAsync(
                "https://music-menu.herokuapp.com/seek",
                new StringContent(requestBody, Encoding.UTF8,
                    "application/json"));
            var message = JsonConvert.DeserializeObject<Message>(await response.Content.ReadAsStringAsync());
            SpotifyMenu.RequestMessage = message!.Text;
        }
        catch (Exception e)
        {
            File.AppendAllLines("mmlog.txt", new []{ e.Message, e.StackTrace, e.InnerException?.Message, e.InnerException?.StackTrace });
            SpotifyMenu.RequestMessage = "Error restarting track.";
        }
        finally
        {
            File.AppendAllLines("mmlog.txt", new []{ SpotifyMenu.RequestMessage });
        }
    }
    
    public static async Task SendRepeatMode(string mode)
    {
        if (Tokens.AccessToken is null or "")
        {
            SpotifyMenu.RequestMessage = "Refresh token not registered.";
            return;
        }

        if (mode != "track" && mode != "context" && mode != "off")
        {
            SpotifyMenu.RequestMessage = "Invalid repeat mode.";
            return;
        }
        
        try
        {
            var requestBody = string.Concat("{\"mode\": \"", mode, "\"}");
            Client.DefaultRequestHeaders.Authorization = 
                AuthenticationHeaderValue.Parse($"Bearer {Tokens.AccessToken}");
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 |
                                                   SecurityProtocolType.Tls12;
            var response = await Client.PutAsync(
                "https://music-menu.herokuapp.com/repeat",
                new StringContent(requestBody, Encoding.UTF8,
                    "application/json"));
            var message = JsonConvert.DeserializeObject<Message>(await response.Content.ReadAsStringAsync());
            SpotifyMenu.RequestMessage = message!.Text;
        }
        catch (Exception e)
        {
            File.AppendAllLines("mmlog.txt", new []{ e.Message, e.StackTrace, e.InnerException?.Message, e.InnerException?.StackTrace });
            SpotifyMenu.RequestMessage = "Error setting playback repeat mode.";
        }
        finally
        {
            File.AppendAllLines("mmlog.txt", new []{ SpotifyMenu.RequestMessage });
        }
    }
    
    public static async Task SendShuffle(bool on)
    {
        if (Tokens.AccessToken is null or "")
        {
            SpotifyMenu.RequestMessage = "Refresh token not registered.";
            return;
        }

        try
        {
            var requestBody = string.Concat("{\"on\": ", JsonConvert.SerializeObject(on), "}");
            Client.DefaultRequestHeaders.Authorization = 
                AuthenticationHeaderValue.Parse($"Bearer {Tokens.AccessToken}");
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 |
                                                   SecurityProtocolType.Tls12;
            var response = await Client.PutAsync(
                "https://music-menu.herokuapp.com/shuffle",
                new StringContent(requestBody, Encoding.UTF8,
                    "application/json"));
            var message = JsonConvert.DeserializeObject<Message>(await response.Content.ReadAsStringAsync());
            SpotifyMenu.RequestMessage = message!.Text;
        }
        catch (Exception e)
        {
            File.AppendAllLines("mmlog.txt", new []{ e.Message, e.StackTrace, e.InnerException?.Message, e.InnerException?.StackTrace });
            SpotifyMenu.RequestMessage = "Error setting playback shuffle mode.";
        }
        finally
        {
            File.AppendAllLines("mmlog.txt", new []{ SpotifyMenu.RequestMessage });
        }
    }
    
    public static async Task SendVolume(int volumePercent)
    {
        if (Tokens.AccessToken is null or "")
        {
            SpotifyMenu.RequestMessage = "Refresh token not registered.";
            return;
        }

        try
        {
            var requestBody = string.Concat("{\"volume_percent\": ", volumePercent, "}");
            Client.DefaultRequestHeaders.Authorization = 
                AuthenticationHeaderValue.Parse($"Bearer {Tokens.AccessToken}");
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 |
                                                   SecurityProtocolType.Tls12;
            var response = await Client.PutAsync(
                "https://music-menu.herokuapp.com/volume",
                new StringContent(requestBody, Encoding.UTF8,
                    "application/json"));
            var message = JsonConvert.DeserializeObject<Message>(await response.Content.ReadAsStringAsync());
            SpotifyMenu.RequestMessage = message!.Text;
        }
        catch (Exception e)
        {
            File.AppendAllLines("mmlog.txt", new []{ e.Message, e.StackTrace, e.InnerException?.Message, e.InnerException?.StackTrace });
            SpotifyMenu.RequestMessage = "Error setting playback volume.";
        }
        finally
        {
            File.AppendAllLines("mmlog.txt", new []{ SpotifyMenu.RequestMessage });
        }
    }
}