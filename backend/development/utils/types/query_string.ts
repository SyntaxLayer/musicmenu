import { z } from "zod";

const CallbackQuery = z.object({
  code: z.string().nullable(),
  state: z.string().nullable(),
});

export type CallbackQuery = z.infer<typeof CallbackQuery>;
