import { z } from "zod";

export const SpotifyError = z.object({
  error: z.object({
    status: z.number().int(),
    message: z.string(),
    reason: z.string().nullish(),
  }),
});

const Device = z.object({
  id: z.string(),
  is_active: z.boolean(),
  is_private_session: z.boolean(),
  is_restricted: z.boolean(),
  name: z.string(),
  volume_percent: z.number().int(),
});
const Item = z.object({
  id: z.string(),
  name: z.string(),
  uri: z.string(),
});

const Playlists = z.object({
  items: Item.array(),
  offset: z.number().int(),
  next: z.string().nullable(),
});

const PlaylistTracks = z.object({
  items: z
    .object({
      track: Item,
    })
    .array(),
  offset: z.number().int(),
  next: z.string().nullable(),
});

const Albums = z.object({
  items: z
    .object({
      album: Item,
    })
    .array(),
  offset: z.number().int(),
  next: z.string().nullable(),
});

const AlbumTracks = z.object({
  items: Item.array(),
  offset: z.number().int(),
  next: z.string().nullable(),
});

const PlayParameters = z.object({
  context_uri: z.string().nullish(),
  uris: z.string().array().nullish(),
});

const SearchParams = z.object({
  match: z.string(),
  tracks: z.boolean(),
  playlists: z.boolean(),
  albums: z.boolean(),
});

export type Device = z.infer<typeof Device>;
export type SpotifyError = z.infer<typeof SpotifyError>;
export type Playlists = z.infer<typeof Playlists>;
export type PlaylistTracks = z.infer<typeof PlaylistTracks>;
export type PlayParameters = z.infer<typeof PlayParameters>;
export type SearchParams = z.infer<typeof SearchParams>;
export type Albums = z.infer<typeof Albums>;
export type AlbumTracks = z.infer<typeof AlbumTracks>;
