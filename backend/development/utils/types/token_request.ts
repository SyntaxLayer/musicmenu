import { z } from "zod";

const TokenRequest = z.object({
  access_token: z.string(),
  refresh_token: z.string(),
});

const TokenAccess = TokenRequest.pick({
  access_token: true,
});

const TokenPair = z.object({
  access_token: z.string(),
  refresh_token: z.string().nullable(),
});

export type TokenRequest = z.infer<typeof TokenRequest>;
export type TokenAccess = z.infer<typeof TokenAccess>;
export type TokenPair = z.infer<typeof TokenPair>;
