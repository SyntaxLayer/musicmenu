import { SpotifyError } from "./types/spotify_requests";

export const parseError = async (errorData?: string): Promise<{ msg: string, status: number }> => {
  if (!errorData) {
    return { msg: "Server error", status: 500 };
  }
  try {
    const parse = await SpotifyError.spa(JSON.parse(errorData));
  
    if (parse.success) {
      return { msg: parse.data.error.message, status: parse.data.error.status };
    }
    else {
      return { msg: errorData, status: 500 };
    }
  } catch (e) {
    console.log(e);
    return { msg: "Server error", status: 500 }; 
  }
};
