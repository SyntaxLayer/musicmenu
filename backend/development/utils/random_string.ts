export const randomString = (length: number): string => {
  let text = "";
  const values =
    "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

  for (let i = 0; i < length; i++) {
    text += values.charAt(Math.floor(Math.random() * values.length));
  }

  return text;
};
