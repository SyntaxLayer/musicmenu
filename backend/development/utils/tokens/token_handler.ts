import axios from "axios";
import { URLSearchParams } from "url";
import { TokenRequest, TokenAccess } from "../types/token_request";

export const requestToken = async (
  auth_code: string
): Promise<TokenRequest | null> => {
  try {
    const params = new URLSearchParams();
    params.append("code", auth_code);
    params.append("redirect_uri", process.env.REDIRECT_URI!);
    params.append("grant_type", "authorization_code");
    params.append("client_id", process.env.CLIENT_ID!);
    params.append("client_secret", process.env.CLIENT_SECRET!);
    const response = await axios.post<TokenRequest>(
      "https://accounts.spotify.com/api/token",
      params.toString(),
      {
        headers: {
          "Content-Type": "application/x-www-form-urlencoded",
        },
      }
    );

    return response.data;
  } catch (err) {
    console.log(err);
    return null;
  }
};

export const refreshToken = async (
  reToken: string | null
): Promise<string | null> => {
  if (!reToken) return null;

  try {
    const params = new URLSearchParams();
    params.append("grant_type", "refresh_token");
    params.append("refresh_token", reToken);
    params.append("client_id", process.env.CLIENT_ID!);
    params.append("client_secret", process.env.CLIENT_SECRET!);
    const response = await axios.post<TokenAccess>(
      "https://accounts.spotify.com/api/token",
      params.toString(),
      {
        headers: {
          "Content-Type": "application/x-www-form-urlencoded",
        },
      }
    );

    return response.data.access_token;
  } catch (err) {
    console.log(err);
    return null;
  }
};
