import fastifyEnv from "@fastify/env";
import fastify from "fastify";
import fastifyCors from "@fastify/cors";
import fastifyHelmet from "@fastify/helmet";
import fastifyCookie from "@fastify/cookie";
import { authRoutes } from "./routes/auth_routes";
import { getTokens } from "./helpers/check_header";
import { devicesRoutes } from "./routes/devices_routes";
import { playbackRoutes } from "./routes/playback_routes";
import { playlistRoutes } from "./routes/playlists.routes";
import { searchRoutes } from "./routes/search_routes";

const schema = {
  type: "object",
  required: [
    "PORT",
    "CLIENT_ID",
    "CLIENT_SECRET",
    "REDIRECT_URI",
    "STATE_KEY",
    "COOKIE_SECRET",
    "SPOTI_HOST",
  ],
  properties: {
    PORT: {
      type: "string",
      default: 8100,
    },
    CLIENT_ID: {
      type: "string",
    },
    CLIENT_SECRET: {
      type: "string",
    },
    REDIRECT_URI: {
      type: "string",
    },
    STATE_KEY: {
      type: "string",
    },
    COOKIE_SECRET: {
      type: "string",
    },
    SPOTI_HOST: {
      type: "string",
    },
  },
};

const options = {
  dotenv: true,
  data: process.env,
  schema,
};

const server = fastify({
  logger: true,
});

const initialize = async () => {
  server.register(fastifyHelmet);
  server.register(fastifyCors);
  server.register(fastifyEnv, options);
  await server.after();
  server.register(fastifyCookie, {
    secret: process.env.COOKIE_SECRET,
    parseOptions: {
      httpOnly: true,
    },
  });
  server.register(authRoutes);
  server.register(devicesRoutes);
  server.register(playbackRoutes);
  server.register(playlistRoutes);
  server.register(searchRoutes);
  server.decorate("authenticate", getTokens);
};

initialize();

(async () => {
  try {
    await server.ready();
    const port = process.env.PORT ?? "8100";
    server.listen(
      {
        port: Number.parseInt(port),
        host: "0.0.0.0",
      },
      (err) => {
        if (err) {
          server.log.error(err);
          return process.exit(1);
        }

        server.log.info(`Server listening in port ${port}`);
      }
    );
  } catch (e) {
    server.log.error(e);
    process.exit(1);
  }
})();
