import { FastifyReply, FastifyRequest } from "fastify";

export const getTokens = async (
  request: FastifyRequest,
  reply: FastifyReply
) => {
  const auth = request.headers.authorization;

  try {
    if (!auth) {
      reply.status(400);
      return { msg: "Auth token missing" };
    }

    if (!RegExp(/^Bearer [a-zA-Z0-9-_]+$/g).test(auth)) {
      reply.status(400);
      return { msg: "Invalid token" };
    }

    request.AuthToken = auth.split("Bearer ")[1];
  } catch (e) {
    console.log(e);
    reply.status(500);
    return { msg: "Error reading token." };
  }
};
