import { FastifyInstance } from "fastify";
import {
  getPlaylists,
  getPlaylistTracks,
  getAlbums,
  getAlbumTracks,
  getSavedTracks,
} from "../controllers/playlists_controllers";

export const playlistRoutes = async (fastify: FastifyInstance) => {
  fastify.get<{ Params: { offset: number } }>(
    "/playlists/:offset",
    { onRequest: fastify.authenticate },
    getPlaylists
  );
  fastify.get<{ Params: { id: string; offset: number } }>(
    "/playlists/:id/:offset/tracks",
    { onRequest: fastify.authenticate },
    getPlaylistTracks
  );
  fastify.get<{ Params: { offset: number } }>(
    "/albums/:offset",
    { onRequest: fastify.authenticate },
    getAlbums
  );
  fastify.get<{ Params: { id: string; offset: number } }>(
    "/albums/:id/:offset/tracks",
    { onRequest: fastify.authenticate },
    getAlbumTracks
  );
  fastify.get<{ Params: { offset: number } }>(
    "/tracks/:offset",
    { onRequest: fastify.authenticate },
    getSavedTracks
  );
};
