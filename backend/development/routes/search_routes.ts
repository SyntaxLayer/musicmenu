import { FastifyInstance } from "fastify";
import { search } from "../controllers/search_controller";
import { SearchParams } from "../utils/types/spotify_requests";

export const searchRoutes = async (fastify: FastifyInstance) => {
    fastify.post<{ Body: SearchParams }>("/search", { onRequest: fastify.authenticate }, search);
};