import { FastifyInstance } from "fastify";
import { callback, login, refresh } from "../controllers/auth_controllers";

export const authRoutes = async (fastify: FastifyInstance) => {
  fastify.get("/login", login);
  fastify.get("/callback", callback);
  fastify.put("/refresh", refresh);
};
