import { FastifyInstance } from "fastify";
import { getDevices, transferDevice } from "../controllers/devices_controllers";

export const devicesRoutes = async (fastify: FastifyInstance) => {
  fastify.get("/devices", { onRequest: fastify.authenticate }, getDevices);

  fastify.put<{
    Body: {
      deviceId: string;
    };
  }>("/transfer", { onRequest: fastify.authenticate }, transferDevice);
};
