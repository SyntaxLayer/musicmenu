import { FastifyInstance } from "fastify";
import {
  pause,
  play,
  skip,
  seek,
  addToQueue,
  repeatMode,
  setShuffle,
  setVolume,
} from "../controllers/playback_controllers";
import { PlayParameters } from "../utils/types/spotify_requests";

export const playbackRoutes = async (fastify: FastifyInstance) => {
  fastify.put("/pause", { onRequest: fastify.authenticate }, pause);

  fastify.put<{ Body: PlayParameters }>(
    "/play",
    { onRequest: fastify.authenticate },
    play
  );

  fastify.post<{ Body: { direction: string } }>(
    "/skip",
    { onRequest: fastify.authenticate },
    skip
  );

  fastify.put<{ Body: { position_ms: number } }>(
    "/seek",
    { onRequest: fastify.authenticate },
    seek
  );

  fastify.post<{ Body: { uri: string } }>(
    "/queue",
    { onRequest: fastify.authenticate },
    addToQueue
  );

  fastify.put<{ Body: { mode: string } }>(
    "/repeat",
    { onRequest: fastify.authenticate },
    repeatMode
  );

  fastify.put<{ Body: { on: boolean } }>(
    "/shuffle",
    { onRequest: fastify.authenticate },
    setShuffle
  );

  fastify.put<{ Body: { volume_percent: number } }>(
    "/volume",
    { onRequest: fastify.authenticate },
    setVolume
  );
};
