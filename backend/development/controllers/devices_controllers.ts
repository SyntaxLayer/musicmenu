import axios, { AxiosError } from "axios";
import { FastifyReply, FastifyRequest } from "fastify";
import { parseError } from "../utils/error_parser";
import { Device } from "../utils/types/spotify_requests";

export const getDevices = async (
  request: FastifyRequest,
  reply: FastifyReply
) => {
  try {
    const response = await axios.get<Device[]>(
      `${process.env.SPOTI_HOST}/me/player/devices`,
      {
        headers: {
          Authorization: `Bearer ${request.AuthToken}`,
        },
      }
    );
    return response.data;
  } catch (err) {
    const error = err as AxiosError;
    console.log(JSON.stringify(error.response?.data));
    reply.status(error.response?.status ?? 500);
    return await parseError(JSON.stringify(error.response?.data));
  }
};

export const transferDevice = async (
  request: FastifyRequest<{ Body: { deviceId: string } }>,
  reply: FastifyReply
) => {
  try {
    await axios.put(
      `${process.env.SPOTI_HOST}/me/player`,
      {
        device_ids: [request.body.deviceId],
      },
      {
        headers: {
          Authorization: `Bearer ${request.AuthToken}`,
        },
      }
    );

    return { msg: "Playback transfered." };
  } catch (err) {
    const error = err as AxiosError;
    console.log(JSON.stringify(error.response?.data));
    reply.status(error.response?.status ?? 500);
    return await parseError(JSON.stringify(error.response?.data));
  }
};
