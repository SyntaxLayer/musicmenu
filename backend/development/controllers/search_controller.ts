import axios, { AxiosError } from "axios";
import { FastifyReply, FastifyRequest } from "fastify";
import { parseError } from "../utils/error_parser";
import { URLSearchParams } from "url";
import { SearchParams } from "../utils/types/spotify_requests";

export const search = async (request: FastifyRequest<{ Body: SearchParams }>, reply: FastifyReply) => {
    try {
        const body = request.body;
        
        if (!body.albums && !body.playlists && !body.tracks) {
            reply.status(400);
            return { msg: "At least one type must be selected." };
        }
        
        const types: string[] = [];
        
        if (body.albums) {
            types.push("album");
        }
        
        if (body.tracks) {
            types.push("track");
        }
        
        if (body.playlists) {
            types.push("playlist");
        }
        
        const params = {
            q: `name:${request.body.match}`,
            type: `${types}`,
        };
        
        const urlParams = new URLSearchParams(params);
        const response = await axios.get(
            `${process.env.SPOTI_HOST}/search?${urlParams.toString()}`, {
                headers: {
                    Authorization: `Bearer ${request.AuthToken}`,
                },
            });
        return response.data;
    } catch (err) {
        const error = err as AxiosError;
        console.log(JSON.stringify(error.response?.data));
        reply.status(error.response?.status ?? 500);
        return await parseError(JSON.stringify(error.response?.data));
    }
}
