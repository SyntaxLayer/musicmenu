import { FastifyReply, FastifyRequest } from "fastify";
import { URLSearchParams } from "url";
import { randomString } from "../utils/random_string";
import { CallbackQuery } from "../utils/types/query_string";
import { refreshToken, requestToken } from "../utils/tokens/token_handler";

export const login = async (request: FastifyRequest, reply: FastifyReply) => {
  const state = randomString(16);
  reply.setCookie(process.env.STATE_KEY!, state, {
    signed: true,
  });

  const scope =
    "user-library-read user-read-email user-read-private user-modify-playback-state user-read-playback-state user-read-currently-playing user-read-playback-position playlist-read-collaborative playlist-read-private";
  const params = {
    response_type: "code",
    client_id: process.env.CLIENT_ID!,
    scope,
    redirect_uri: process.env.REDIRECT_URI!,
    state,
  };
  const urlParams = new URLSearchParams(params);
  reply.redirect(
    `https://accounts.spotify.com/authorize?${urlParams.toString()}`
  );
};

export const callback = async (
  request: FastifyRequest<{ Querystring: CallbackQuery }>,
  reply: FastifyReply
) => {
  const { code, state } = request.query;
  const storedState = request.cookies
    ? request.cookies[process.env.STATE_KEY!]
    : null;

  if (state === null || storedState === null) {
    return reply.redirect("/#?error=state_mismatch");
  }

  const usingnedState = request.unsignCookie(storedState);

  if (usingnedState.value !== state) {
    reply.status(400);
    return { msg: "State mismatch." };
  }

  reply.clearCookie(process.env.STATE_KEY!);
  const tokens = await requestToken(code!);

  if (tokens) {
    reply.type("text/html");
    return reply.send(`
    <!DOCTYPE html>
    <html lang="eng">
        <body style="margin: 0;">
            <div style="background: black; bottom:0; left:-50%; opacity: 90%; position:fixed; right:-50%; 
                top:0; z-index:-1;">
            </div>
            <div style="font-family: 'Yu Gothic UI',serif; background-color:rgba(30, 215, 96,75%); border-radius:.25em;
                box-shadow:0 0 .25em rgba(0,0,0,.25);box-sizing:border-box; left:50%; padding:10vmin; position:fixed;
                text-align:center; top:50%; transform:translate(-50%, -50%);">
                <p style="font-weight: bold; font-size: large;">
                    Spotify Access granted.
                </p>
                <p style="flex-wrap: wrap; line-break: anywhere;">
                    <b>Access token</b>:<br/><i>${tokens.access_token}</i>
                </p>
                <p style="flex-wrap: wrap; line-break: anywhere;">
                    <b>Refresh token</b>:<br/><i>${tokens.refresh_token}</i>
                </p>
            </div>
        </body>
    </html>
    `);
  }

  reply.status(500);
  return { msg: "Spotify access denied." };
};

export const refresh = async (
  request: FastifyRequest<{ Body: { refreshToken: string } }>,
  reply: FastifyReply
) => {
  const response = await refreshToken(request.body.refreshToken);

  if (!response) {
    reply.status(500);
    return { msg: "Token request failed." };
  }

  return { msg: "New token received.", token: response };
};
