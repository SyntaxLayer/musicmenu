import axios, { AxiosError } from "axios";
import { FastifyRequest, FastifyReply } from "fastify";
import { parseError } from "../utils/error_parser";
import { URLSearchParams } from "url";
import { PlayParameters } from "../utils/types/spotify_requests";

export const pause = async (request: FastifyRequest, reply: FastifyReply) => {
  try {
    await axios.put(`${process.env.SPOTI_HOST}/me/player/pause`, undefined, {
      headers: {
        Authorization: `Bearer ${request.AuthToken}`,
      },
    });

    return { msg: "Playback paused." };
  } catch (err) {
    const error = err as AxiosError;
    console.log(JSON.stringify(error.response?.data));
    reply.status(error.response?.status ?? 500);
    return await parseError(JSON.stringify(error.response?.data));
  }
};

export const play = async (
  request: FastifyRequest<{ Body: PlayParameters }>,
  reply: FastifyReply
) => {
  try {
    await axios.put(
      `${process.env.SPOTI_HOST}/me/player/play`,
      {
        context_uri: request.body.context_uri,
        uris: request.body.uris,
        position_ms: 0,
      },
      {
        headers: {
          Authorization: `Bearer ${request.AuthToken}`,
        },
      }
    );

    return { msg: "Playback resumed." };
  } catch (err) {
    const error = err as AxiosError;
    console.log(JSON.stringify(error.response?.data));
    reply.status(error.response?.status ?? 500);
    return await parseError(JSON.stringify(error.response?.data));
  }
};

export const skip = async (
  request: FastifyRequest<{ Body: { direction: string } }>,
  reply: FastifyReply
) => {
  try {
    const direction = request.body.direction;

    if (direction !== "previous" && direction !== "next") {
      reply.status(400);
      return { msg: "Skip direction invalid." };
    }

    await axios.post(
      `${process.env.SPOTI_HOST}/me/player/${direction}`,
      undefined,
      {
        headers: {
          Authorization: `Bearer ${request.AuthToken}`,
        },
      }
    );

    return { msg: `Skipped to ${direction}` };
  } catch (err) {
    const error = err as AxiosError;
    console.log(JSON.stringify(error.response?.data));
    reply.status(error.response?.status ?? 500);
    return await parseError(JSON.stringify(error.response?.data));
  }
};

export const seek = async (
  request: FastifyRequest<{ Body: { position_ms: number } }>,
  reply: FastifyReply
) => {
  try {
    const position_ms = request.body.position_ms;

    if (position_ms < 0) {
      reply.status(400);
      return { msg: "Invalid position." };
    }

    const params = {
      position_ms: position_ms.toString(),
    };
    const urlParams = new URLSearchParams(params);

    await axios.put(
      `${process.env.SPOTI_HOST}/me/player/seek?${urlParams.toString()}`,
      undefined,
      {
        headers: {
          Authorization: `Bearer ${request.AuthToken}`,
        },
      }
    );

    return { msg: `Skipped to ${position_ms}` };
  } catch (err) {
    const error = err as AxiosError;
    console.log(JSON.stringify(error));
    console.log(JSON.stringify(error.response?.data));
    reply.status(error.response?.status ?? 500);
    return await parseError(JSON.stringify(error.response?.data));
  }
};

export const addToQueue = async (
  request: FastifyRequest<{ Body: { uri: string } }>,
  reply: FastifyReply
) => {
  try {
    const params = {
      uri: request.body.uri,
    };
    const urlParams = new URLSearchParams(params);
    await axios.post(
      `${process.env.SPOTI_HOST}/me/player/queue?${urlParams.toString()}`,
      undefined,
      {
        headers: {
          Authorization: `Bearer ${request.AuthToken}`,
        },
      }
    );

    return { msg: `Song queued.` };
  } catch (err) {
    const error = err as AxiosError;
    console.log(JSON.stringify(error.response?.data));
    reply.status(error.response?.status ?? 500);
    return await parseError(JSON.stringify(error.response?.data));
  }
};

export const repeatMode = async (
  request: FastifyRequest<{ Body: { mode: string } }>,
  reply: FastifyReply
) => {
  try {
    const mode = request.body.mode;

    if (mode !== "track" && mode !== "context" && mode !== "off") {
      reply.status(400);
      return { msg: "Invalid repeat mode received." };
    }

    const params = {
      state: mode,
    };
    const urlParams = new URLSearchParams(params);

    await axios.put(
      `${process.env.SPOTI_HOST}/me/player/repeat?${urlParams.toString()}`,
      undefined,
      {
        headers: {
          Authorization: `Bearer ${request.AuthToken}`,
        },
      }
    );

    return { msg: `Repeat mode set to ${mode}` };
  } catch (err) {
    const error = err as AxiosError;
    console.log(JSON.stringify(error.response?.data));
    reply.status(error.response?.status ?? 500);
    return await parseError(JSON.stringify(error.response?.data));
  }
};

export const setShuffle = async (
  request: FastifyRequest<{ Body: { on: boolean } }>,
  reply: FastifyReply
) => {
  try {
    const on = request.body.on;
    const params = {
      state: `${on}`,
    };
    const urlParams = new URLSearchParams(params);

    await axios.put(
      `${process.env.SPOTI_HOST}/me/player/shuffle?${urlParams.toString()}`,
      undefined,
      {
        headers: {
          Authorization: `Bearer ${request.AuthToken}`,
        },
      }
    );

    return { msg: `Playback shuffle ${on ? "activated" : "deactivated"}` };
  } catch (err) {
    const error = err as AxiosError;
    console.log(JSON.stringify(error.response?.data));
    reply.status(error.response?.status ?? 500);
    return await parseError(JSON.stringify(error.response?.data));
  }
};

export const setVolume = async (
  request: FastifyRequest<{ Body: { volume_percent: number } }>,
  reply: FastifyReply
) => {
  try {
    const volume_percent = request.body.volume_percent;
    const params = {
      volume_percent: volume_percent.toString(),
    };
    const urlParams = new URLSearchParams(params);

    await axios.put(
      `${process.env.SPOTI_HOST}/me/player/volume?${urlParams.toString()}`,
      undefined,
      {
        headers: {
          Authorization: `Bearer ${request.AuthToken}`,
        },
      }
    );

    return { msg: `Playback volume set to ${volume_percent}%` };
  } catch (err) {
    const error = err as AxiosError;
    console.log(JSON.stringify(error.response?.data));
    reply.status(error.response?.status ?? 500);
    return await parseError(JSON.stringify(error.response?.data));
  }
};
