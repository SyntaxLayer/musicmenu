import axios, { AxiosError } from "axios";
import { FastifyReply, FastifyRequest } from "fastify";
import { parseError } from "../utils/error_parser";
import { URLSearchParams } from "url";
import {
  Albums,
  AlbumTracks,
  Playlists,
  PlaylistTracks,
} from "../utils/types/spotify_requests";

export const getPlaylists = async (
  request: FastifyRequest<{ Params: { offset: number } }>,
  reply: FastifyReply
) => {
  try {
    const params = {
      offset: request.params.offset.toString(),
      limit: "50",
    };
    const urlParams = new URLSearchParams(params);
    const response = await axios.get<Playlists>(
      `${process.env.SPOTI_HOST}/me/playlists?${urlParams.toString()}`,
      {
        headers: {
          Authorization: `Bearer ${request.AuthToken}`,
        },
      }
    );

    const items = response.data.items.map((value) => {
      return {
        id: value.id,
        name: value.name,
        uri: value.uri,
      };
    });

    return {
      items,
      offset: response.data.offset,
      next: response.data.next !== null,
    };
  } catch (err) {
    const error = err as AxiosError;
    console.log(JSON.stringify(error));
    console.log(JSON.stringify(error.response?.data));
    reply.status(error.response?.status ?? 500);
    return await parseError(JSON.stringify(error.response?.data));
  }
};

export const getPlaylistTracks = async (
  request: FastifyRequest<{ Params: { id: string; offset: number } }>,
  reply: FastifyReply
) => {
  try {
    const { id, offset } = request.params;
    const params = {
      offset: offset.toString(),
      limit: "50",
    };
    const urlParams = new URLSearchParams(params);
    const response = await axios.get<PlaylistTracks>(
      `${
        process.env.SPOTI_HOST
      }/playlists/${id}/tracks?${urlParams.toString()}`,
      {
        headers: {
          Authorization: `Bearer ${request.AuthToken}`,
        },
      }
    );
    const items = response.data.items.map((value) => {
      return {
        id: value.track.id,
        name: value.track.name,
        uri: value.track.uri,
      };
    });

    return {
      items,
      offset: response.data.offset,
      next: response.data.next !== null,
    };
  } catch (err) {
    const error = err as AxiosError;
    console.log(JSON.stringify(error));
    console.log(JSON.stringify(error.response?.data));
    reply.status(error.response?.status ?? 500);
    return await parseError(JSON.stringify(error.response?.data));
  }
};

export const getAlbums = async (
  request: FastifyRequest<{ Params: { offset: number } }>,
  reply: FastifyReply
) => {
  try {
    const params = {
      offset: request.params.offset.toString(),
      limit: "50",
    };
    const urlParams = new URLSearchParams(params);
    const response = await axios.get<Albums>(
      `${process.env.SPOTI_HOST}/me/albums?${urlParams.toString()}`,
      {
        headers: {
          Authorization: `Bearer ${request.AuthToken}`,
        },
      }
    );
    const items = response.data.items.map((value) => {
      return {
        id: value.album.id,
        name: value.album.name,
        uri: value.album.uri,
      };
    });

    return {
      items,
      offset: response.data.offset,
      next: response.data.next !== null,
    };
  } catch (err) {
    const error = err as AxiosError;
    console.log(JSON.stringify(error));
    console.log(JSON.stringify(error.response?.data));
    reply.status(error.response?.status ?? 500);
    return await parseError(JSON.stringify(error.response?.data));
  }
};

export const getAlbumTracks = async (
  request: FastifyRequest<{ Params: { id: string; offset: number } }>,
  reply: FastifyReply
) => {
  try {
    const { id, offset } = request.params;
    const params = {
      offset: offset.toString(),
      limit: "50",
    };
    const urlParams = new URLSearchParams(params);
    const response = await axios.get<AlbumTracks>(
      `${process.env.SPOTI_HOST}/albums/${id}/tracks?${urlParams.toString()}`,
      {
        headers: {
          Authorization: `Bearer ${request.AuthToken}`,
        },
      }
    );
    const items = response.data.items.map((value) => {
      return {
        id: value.id,
        name: value.name,
        uri: value.uri,
      };
    });

    return {
      items,
      offset: response.data.offset,
      next: response.data.next !== null,
    };
  } catch (err) {
    const error = err as AxiosError;
    console.log(JSON.stringify(error));
    console.log(JSON.stringify(error.response?.data));
    reply.status(error.response?.status ?? 500);
    return await parseError(JSON.stringify(error.response?.data));
  }
};

export const getSavedTracks = async (
  request: FastifyRequest<{ Params: { offset: number } }>,
  reply: FastifyReply
) => {
  try {
    const params = {
      offset: request.params.offset.toString(),
      limit: "50",
    };
    const urlParams = new URLSearchParams(params);
    const response = await axios.get<PlaylistTracks>(
      `${process.env.SPOTI_HOST}/me/tracks?${urlParams.toString()}`,
      {
        headers: {
          Authorization: `Bearer ${request.AuthToken}`,
        },
      }
    );
    const items = response.data.items.map((value) => {
      return {
        id: value.track.id,
        name: value.track.name,
        uri: value.track.uri,
      };
    });

    return {
      items,
      offset: response.data.offset,
      next: response.data.next !== null,
    };
  } catch (err) {
    const error = err as AxiosError;
    console.log(JSON.stringify(error));
    console.log(JSON.stringify(error.response?.data));
    reply.status(error.response?.status ?? 500);
    return await parseError(JSON.stringify(error.response?.data));
  }
};
